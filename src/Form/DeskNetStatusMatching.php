<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ActionController;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\DeleteMethods;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Desk-Net status matching settings.
 */
class DeskNetStatusMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetStatusMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, DeleteMethods $delete_methods, TranslationInterface $string_translation, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->deleteMethods = $delete_methods;
    $this->stringTranslation = $string_translation;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('desk_net.delete_methods'),
    $container->get('string_translation'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_status_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration_setting_name = 'desk_net_selected_status_matching_list';
    $loaded_desk_net_statuses = $this->getDeskNetStatuses();
    $platform_id = $this->configFactory->get('desk_net.settings')->get('platform_id');

    if (!empty($platform_id)) {
      if (!empty(ModuleSettings::variableGet('desk_net_token')) && $loaded_desk_net_statuses) {
        $html = '<h2>' . $this->stringTranslation->translate('Status Matching') . '</h2>';
        $html .= $this->stringTranslation->translate('Use this page to match publication statuses in Desk-Net to those in Drupal and vice versa.');

        $form['html'] = [
          '#markup' => $html,
        ];

        $initial_status_syncing = (bool) $this->configFactory->get('desk_net.settings')
          ->get('initial_status_syncing') ?? FALSE;

        $form['initial_status_syncing'] = [
          '#type' => 'checkbox',
          '#title' => $this->stringTranslation->translate('Only allow initial status syncing'),
          '#default_value' => $initial_status_syncing,
          '#required' => FALSE,
        ];

        $sub_title_dn_to_drupal = '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Desk-Net to Drupal') . '</h4>';

        $form['subTitleDNToDrupal'] = [
          '#markup' => $sub_title_dn_to_drupal,
        ];

        $desk_net_to_drupal = $loaded_desk_net_statuses['activated'];
        $desk_net_to_drupal['desk_net_removed']['id'] = 'desk_net_removed';
        $desk_net_to_drupal['desk_net_removed']['name'] = $this->stringTranslation->translate('Deleted/removed');

        $drupal_to_desk_net = [
        [
          'id' => 0,
          'name' => $this->stringTranslation->translate('Unpublished'),
        ],
        [
          'id' => 1,
          'name' => $this->stringTranslation->translate('Published'),
        ],
        ];

        $field_name = ActionController::generateFieldMatchingName('status', 'desk_net_to_drupal', '5');

        $matching_value = ActionController::getFieldMatchingValue($configuration_setting_name, $field_name);

        if (empty($matching_value) && $matching_value != 0) {
          ActionController::setFieldMatchingValue($configuration_setting_name, $field_name, '1');
        }

        $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
        'status', 'desk_net_to_drupal', 0
        );

        unset($desk_net_to_drupal['desk_net_removed']);

        if (!empty($loaded_desk_net_statuses['deactivated'])) {
          foreach ($loaded_desk_net_statuses['deactivated'] as $value) {
            $desk_net_to_drupal[] = $value;
          }
        }

        $sub_title_drupal_to_dn = '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

        $form['subTitleDrupalToDN'] = [
          '#markup' => $sub_title_drupal_to_dn,
        ];

        $field_name = ActionController::generateFieldMatchingName('status', 'drupal_to_desk_net', '1');

        $desk_net_to_drupal['no_sync'] = [
          'id' => 'no_sync',
          'name' => $this->stringTranslation->translate('No Sync'),
        ];

        $matching_value = ActionController::getFieldMatchingValue($configuration_setting_name, $field_name);

        if (empty($matching_value) && $matching_value != 0) {
          ActionController::setFieldMatchingValue($configuration_setting_name, $field_name, '5');
        }

        $form['drupal_to_desk_net_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $drupal_to_desk_net, $desk_net_to_drupal,
        'status', 'drupal_to_desk_net', 1
        );

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->stringTranslation->translate('Save'),
        ];

        return $form;
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit') {
          $formValues[$key] = $value;
        }
      }

      $initial_status_syncing = $form_state->getValue('initial_status_syncing');

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_status_matching_list', serialize($formValues))
        ->set('initial_status_syncing', $initial_status_syncing)
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Perform get Status list from Desk-Net.
   *
   * @return bool|array
   *   The result loading status list from Desk-Net.
   */
  private function getDeskNetStatuses() {
    $desk_net_to_drupal = [];
    $config = $this->configFactory->get('desk_net.settings');
    $platform_id = $config->get('platform_id');

    if (!empty($platform_id)) {
      $desk_net_status_list = $this->requestsController->get(ModuleSettings::DN_BASE_URL,
      'publication-status/platform', $platform_id);

      if ($desk_net_status_list === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(9));

        return FALSE;
      }

      $desk_net_status_list = json_decode($desk_net_status_list, TRUE);

      if (!empty($desk_net_status_list['message']) || $desk_net_status_list === 'not_show_new_notice'
         || empty($desk_net_status_list)) {
        return FALSE;
      }

      $desk_net_to_drupal['activated'] = array_filter($desk_net_status_list, function ($item) {
        return isset($item['active']) && $item['active'] === TRUE;
      });
      $desk_net_to_drupal['deactivated'] = array_filter($desk_net_status_list, function ($item) {
        return isset($item['active']) && $item['active'] === FALSE;
      });

      $saved_activated_status_list = $config->get('desk_net_activate_status_list');
      $saved_status_list_in_configuration_settings = [];

      if ($saved_activated_status_list !== NULL) {
        $saved_status_list_in_configuration_settings = unserialize($saved_activated_status_list, ['allowed_classes' => FALSE]);
      }

      $new_status_list_from_desk_net = $desk_net_to_drupal['activated'];
      // Unpublished and Published statuses.
      $drupal_status_list = [0, 1];
      // Remove incorrect status matching.
      $this->deleteMethods->shapeDeletedItems('desk_net_selected_status_matching_list',
      $new_status_list_from_desk_net, $saved_status_list_in_configuration_settings, $drupal_status_list, 'status', FALSE);

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_activate_status_list', serialize($desk_net_to_drupal['activated']))
        ->set('desk_net_deactivate_status_list', serialize($desk_net_to_drupal['deactivated']))
        ->save();
    }

    if (empty($desk_net_to_drupal)) {
      $desk_net_activate_status_list = $config->get('desk_net_activate_status_list');
      $desk_net_deactivate_status_list = $config->get('desk_net_deactivate_status_list');

      if ($desk_net_activate_status_list !== NULL) {
        $desk_net_to_drupal['activated'] = unserialize($desk_net_activate_status_list, ['allowed_classes' => FALSE]);
      }

      if ($desk_net_deactivate_status_list !== NULL) {
        $desk_net_to_drupal['deactivated'] = unserialize($desk_net_deactivate_status_list, ['allowed_classes' => FALSE]);
      }
    }

    return (!empty($desk_net_to_drupal['activated'])
      || !empty($desk_net_to_drupal['deactivated'])) ? $desk_net_to_drupal : FALSE;
  }

}
