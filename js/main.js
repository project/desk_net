(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.desk_net = {
    attach: function attach() {
      const generateNewCredentialsBtn = $('#generate-new-credentials-submit');
      const confirmationPopUp = $('#consentModal');
      const closeBtnList = $('#consentModal .close, #consentModal #cancel');

      // When the user clicks on (x), close the confirmation popup.
      $.each(closeBtnList, function () {
        $(this).click(function () {
          confirmationPopUp.css('display', 'none');
        });
      });
      // Lock Submit.
      generateNewCredentialsBtn.submit();

      generateNewCredentialsBtn.click(function () {
        confirmationPopUp.css('display', 'block');
      });
    }
  };
})(jQuery, Drupal);
