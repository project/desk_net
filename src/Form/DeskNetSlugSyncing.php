<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure Desk-Net slug synchronization settings.
 */
class DeskNetSlugSyncing extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Constructs a DeskNetSlugSyncing form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_slug_syncing';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $link_slug_feature = Link::fromTextAndUrl(
    $this->stringTranslation->translate('slug feature'),
    Url::fromUri("http://support.desk-net.com/hc/en-us/articles/115003545092", ['attributes' => ['target' => '_blank']])
    )->toString();

    // Slug field Desk-Net.
    $desk_net_slug['slug']['id'] = 'slug_syncing';
    $desk_net_slug['slug']['name'] = $this->stringTranslation->translate('Slug (Desk-Net)');

    // Set values.
    $desk_net_slug_value['desk_net_slug_permalink_val']['id'] = 'url_alias';
    $desk_net_slug_value['desk_net_slug_permalink_val']['name'] = $this->stringTranslation->translate('URL Alias');
    $desk_net_slug_value['desk_net_slug_headline_val']['id'] = 'title';
    $desk_net_slug_value['desk_net_slug_headline_val']['name'] = $this->stringTranslation->translate('Title');

    $html = '<h2>' . $this->stringTranslation->translate('Slug Syncing') . '</h2>';
    $html .= '<p>';
    $html .= $this->stringTranslation->translate('Desk-Net syncs the Title, SEO Title and the URL Alias with the @link in Desk-Net. Please define what field should be primarily synced to Desk-Net in case of conflicts.', ['@link' => $link_slug_feature]);
    $html .= '</p>';

    $form['html'] = [
      '#markup' => $html,
    ];

    $form['desk_net_slug_syncing'] = PageTemplate::generateDeskNetMatchingPageTemplate(
    'desk_net_slug_syncing', $desk_net_slug, $desk_net_slug_value, '', '', 'url_alias'
    );

    $initial_slug_syncing = (bool) $this->configFactory->get('desk_net.settings')->get('initial_slug_syncing') ?? FALSE;

    $form['initial_slug_syncing'] = [
      '#type' => 'checkbox',
      '#title' => $this->stringTranslation->translate('Only allow initial slug syncing'),
      '#default_value' => $initial_slug_syncing,
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->stringTranslation->translate('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          // Save value.
          $formValues[$key] = $value;
        }
      }

      $initial_slug_syncing = $form_state->getValue('initial_slug_syncing');

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_slug_syncing', serialize($formValues))
        ->set('initial_slug_syncing', $initial_slug_syncing)
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

}
