<?php

namespace Drupal\desk_net;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ActionController;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides methods for deleting entities related to Desk-Net integration.
 */
class DeleteMethods extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeleteMethods object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the RequestsController instance.
   *
   * This method is used to inject the RequestsController dependency into this
   * ActionController instance. This approach, known as setter injection,
   * helps to resolve circular dependencies by setting the dependency after
   * the object has been instantiated.
   *
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The RequestsController instance to be set.
   */
  public function setRequestsController(RequestsController $requests_controller) {
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('entity_type.manager')
    );

    $instance->setRequestsController($container->get('desk_net.requests_controller'));
    return $instance;
  }

  /**
   * Perform shape list of deleted items.
   *
   * @param string $setting_name
   *   The name of the setting in the configuration.
   * @param array $new_items_list
   *   The new element list.
   * @param array $save_items_list
   *   The save element list in Drupal Configuration Settings.
   * @param array $drupal_items_list
   *   The basic Drupal elements for Node.
   *   array[key] string
   *   The key has ID or Slug element value.
   * @param string $type
   *   The type elements in list.
   * @param bool $clear_both_direction
   *   Flag to delete both directions.
   */
  public function shapeDeletedItems(
    $setting_name,
    array $new_items_list,
    array $save_items_list,
    array $drupal_items_list,
    $type,
    $clear_both_direction = TRUE,
  ) {
    $delete_element_list = [];

    if (!empty($save_items_list)) {
      foreach ($save_items_list as $value) {
        $in_stock = FALSE;
        foreach ($new_items_list as $content) {
          if ($value["id"] == $content["id"]) {
            $in_stock = TRUE;
          }
        }

        if (!$in_stock) {
          $delete_element_list[] = $value['id'];
        }
      }
    }

    if (!empty($delete_element_list)) {
      $delete_element_list = $this->checkSubItems($delete_element_list, $save_items_list);
      $this->deleteItems($setting_name, $delete_element_list, $drupal_items_list, $type, $clear_both_direction);
    }
  }

  /**
   * Perform checking parent elements in the list of elements to be deleted.
   *
   * @param array $delete_element_list
   *   The element for delete from saved list.
   * @param array $save_items_list
   *   The save element list in Drupal DB.
   *
   * @return array
   *   The list with deleting elements.
   */
  private function checkSubItems(array $delete_element_list, array $save_items_list) {
    $under_sub_category = [];

    if (!empty($save_items_list)) {
      foreach ($save_items_list as $item) {
        foreach ($delete_element_list as $value) {
          if (isset($item["category"]) && $value == $item["category"]) {
            $delete_element_list[] = $item['id'];
            $under_sub_category[] = $item['id'];
          }
        }
      }
    }

    if (!empty($under_sub_category)) {
      return $this->checkSubItems($under_sub_category, $save_items_list);
    }
    else {
      return $delete_element_list;
    }
  }

  /**
   * Perform deleted items and mapping category.
   *
   * @param string $setting_name
   *   The name of the setting in the configuration.
   * @param array $delete_element_list
   *   The element for delete from saved list.
   * @param array $drupal_items_list
   *   The basic Drupal elements for Node.
   * @param string $type
   *   The type elements in list.
   * @param bool $clear_both_direction
   *   A boolean indicating whether to clear in both directions.
   */
  private function deleteItems($setting_name, array $delete_element_list, array $drupal_items_list, $type, $clear_both_direction) {
    if (!empty($drupal_items_list) && !empty($delete_element_list)) {
      $value_list = $this->configFactory->get('desk_net.settings')->get($setting_name);

      if ($value_list !== NULL) {
        $value_list = unserialize($value_list, ['allowed_classes' => FALSE]);
      }

      foreach ($delete_element_list as $value) {
        foreach ($drupal_items_list as $key) {
          $field_name = ActionController::generateFieldMatchingName($type, 'drupal_to_desk_net', $key);
          if (isset($value_list[$field_name]) && $value == $value_list[$field_name] && $clear_both_direction) {
            $value_list[$field_name] = 'no_' . $type;
          }
        }
        $field_name = ActionController::generateFieldMatchingName($type, 'desk_net_to_drupal', $value);
        unset($value_list[$field_name]);
      }

      $this->configFactory->getEditable('desk_net.settings')
        ->set($setting_name, serialize($value_list))
        ->save();
    }
  }

  /**
   * Deleting Desk-Net module fields.
   *
   * @param string $field_name
   *   The field name.
   * @param string $entity_type
   *   The entity type.
   * @param string $delete_by
   *   The type deleting element.
   */
  private function deleteCustomField($field_name, $entity_type = 'node', $delete_by = 'by_hash') {
    switch ($delete_by) {
      case 'by_name':
        if (FieldStorageConfig::loadByName($entity_type, $field_name) !== NULL) {
          FieldStorageConfig::loadByName($entity_type, $field_name)->delete();
        }
        break;

      default:
        // Getting hash name for field.
        $hash_field = ModuleSettings::variableGet($field_name);
        // Deleting matching field.
        ModuleSettings::variableDel($field_name);

        // Deleting field.
        if ($hash_field !== NULL && FieldStorageConfig::loadByName($entity_type, $hash_field) !== NULL) {
          FieldStorageConfig::loadByName($entity_type, $hash_field)->delete();
        }
    }
  }

  /**
   * Deleting node in Drupal 9.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The node that is being deleted.
   *
   * @return bool
   *   The result of deleting article in Drupal.
   */
  public function deleteElement(EntityInterface $entity) {
    // Deleting variable date for article.
    if (ModuleSettings::variableGet("desk-net-node-" . $entity->vid->value . "-date") !== NULL) {
      ModuleSettings::variableDel("desk-net-node-" . $entity->vid->value . "-date");
    }
    // Getting Desk-Net revision data.
    $desk_net_revision = ModuleSettings::deskNetRevisionGet($entity);

    if (isset($desk_net_revision['desk_net_story_id'])) {
      // Check Deleted/Removed Status.
      if (!empty($desk_net_revision['desk_net_removed_status']) && $desk_net_revision['desk_net_removed_status'] === 'desk_net_removed') {
        return FALSE;
      }
      // Check exist story on Desk-Net side.
      $story_data = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'elements', $desk_net_revision['desk_net_story_id']);

      if ($story_data === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(3));

        return FALSE;
      }

      $story_data = json_decode($story_data, TRUE);

      if ($story_data === 'not_show_new_notice' || empty($story_data) || !empty($story_data['message'])) {
        return FALSE;
      }

      if (isset($desk_net_revision['desk_net_publications_id']) && !empty($desk_net_revision['desk_net_publications_id'])) {
        if (ModuleSettings::checkUserCredentials()) {
          $this->requestsController->customRequest('DELETE', [], ModuleSettings::DN_BASE_URL, 'elements/publication', $desk_net_revision['desk_net_publications_id']);
        }
        else {
          $this->messenger->addMessage([
            '#markup' => NoticesCollection::getNotice(5),
            '#allowed_tags' => ['a'],
          ], 'error');
        }
      }
    }
  }

}
