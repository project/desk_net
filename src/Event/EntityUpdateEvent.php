<?php

namespace Drupal\desk_net\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Event that is fired when an entity will be inserted/updated/deleted.
 */
class EntityUpdateEvent extends Event {

  const EVENT_NAME = 'desk_net.subscriber';

  /**
   * The entity being updated.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  public EntityInterface $entity;

  /**
   * The action being performed on the entity.
   *
   * @var string
   */
  public string $action;

  /**
   * Constructs the object.
   */
  public function __construct(EntityInterface $entity, $action) {
    $this->entity = $entity;
    $this->action = $action;
  }

}
