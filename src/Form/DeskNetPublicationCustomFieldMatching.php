<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\DeleteMethods;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Publication custom field matching settings.
 */
class DeskNetPublicationCustomFieldMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetPublicationCustomFieldMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation, DeleteMethods $delete_methods, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->deleteMethods = $delete_methods;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation'),
    $container->get('desk_net.delete_methods'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_publication_custom_field_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Loading all Drupal Content types from Desk-Net.
    $is_publication_custom_fields_exist = $this->getPublicationCustomFields();
    $config = $this->configFactory->get('desk_net.settings');
    $configuration_setting_name = 'desk_net_selected_publication_custom_field_matching_list';
    $publication_cf_list = $config->get('desk_net_publication_custom_fields');

    if ($publication_cf_list !== NULL) {
      $publication_cf_list = unserialize($publication_cf_list, ['allowed_classes' => FALSE]);
    }

    if (!empty($config->get('platform_id'))) {
      $html = '<h2>' . $this->stringTranslation->translate('Publication Custom Field Matching') . '</h2>';
      $html .= '<p>';
      $html .= $this->stringTranslation->translate('Use this page to match the publication custom fields in Desk-Net with the Title and URL alias in Drupal.');
      $html .= '</p>';

      if ($is_publication_custom_fields_exist && !empty(ModuleSettings::variableGet('desk_net_token'))) {
        $drupal_to_desk_net = [
          'title' => [
            'id' => 'title',
            'name' => $this->stringTranslation->translate('Title'),
          ],
          'url_alias' => [
            'id' => 'url_alias',
            'name' => $this->stringTranslation->translate('URL Alias'),
          ],
          'no_publication_cf' => [
            'id' => 'no_publication_cf',
            'name' => $this->stringTranslation->translate('No Sync'),
          ],
        ];

        if (!empty($publication_cf_list)) {
          foreach ($publication_cf_list as $value) {
            $desk_net_to_drupal[$value['id']]['id'] = $value['id'];
            $desk_net_to_drupal[$value['id']]['name'] = $value['name'];
          }
        }

        $html .= '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Desk-Net to Drupal') . '</h4>';

        $form['html'] = [
          '#markup' => $html,
        ];

        $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
        'publication_cf', 'desk_net_to_drupal', 'no_publication_cf'
        );

        $sub_title = '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

        $form['subTitle'] = [
          '#markup' => $sub_title,
        ];

        unset($drupal_to_desk_net['no_publication_cf']);
        $desk_net_to_drupal['no_publication_cf'] = [
          'id' => 'no_publication_cf',
          'name' => $this->stringTranslation->translate('No Sync'),
        ];

        $form['drupal_to_desk_net_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $drupal_to_desk_net, $desk_net_to_drupal,
        'publication_cf', 'drupal_to_desk_net', 'no_publication_cf'
        );

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->stringTranslation->translate('Save'),
        ];

        return $form;
      }
      elseif (!$is_publication_custom_fields_exist && empty($publication_cf_list)) {
        $html .= '<p>';
        $html .= '<strong>' . $this->stringTranslation->translate('Please note:') . '</strong> ';
        $html .= $this->stringTranslation->translate('The synchronized platform in Desk-Net lacks publication custom fields for matching.');
        $html .= '</p>';

        $form['html'] = [
          '#markup' => $html,
        ];

        return $form;
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          $formValues[$key] = $value;
        }
      }

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_publication_custom_field_matching_list', serialize($formValues))
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Perform get Types for Desk-Net platform.
   *
   * @return bool
   *   The result loading types list from Desk-Net.
   */
  private function getPublicationCustomFields() {
    $config = $this->configFactory->get('desk_net.settings');
    $configFactory = $this->configFactory->getEditable('desk_net.settings');
    $platform_id = $config->get('platform_id');
    $saved_publication_cf_list = $config->get('desk_net_publication_custom_fields');
    $drupal_sync_field_list = [
      'title',
      'url_alias',
      'no_publication_cf',
    ];

    if ($saved_publication_cf_list !== NULL) {
      $saved_publication_cf_list = unserialize($saved_publication_cf_list, ['allowed_classes' => FALSE]);
    }

    if (!empty($platform_id)) {
      $new_publication_cf_list = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'publication-custom-fields', NULL, 'v1_0_0');

      if ($new_publication_cf_list === 'unauthorized') {
        return FALSE;
      }

      if (!empty($new_publication_cf_list['message']) || $new_publication_cf_list === 'not_show_new_notice') {
        return FALSE;
      }

      $new_publication_cf_list = json_decode($new_publication_cf_list, TRUE);
      $new_publication_cf_list = array_filter($new_publication_cf_list, function ($item) {
        return isset($item['active']) && $item['active'] === TRUE && $item['type'] === 1;
      });

      if (empty($new_publication_cf_list)) {
        $configFactory->set('desk_net_publication_custom_fields', serialize($new_publication_cf_list))
          ->save();
        $configFactory->set('desk_net_selected_publication_custom_field_matching_list', serialize([]))
          ->save();
        return FALSE;
      }

      $this->deleteMethods->shapeDeletedItems(
      'desk_net_selected_publication_custom_field_matching_list',
      $new_publication_cf_list,
      $saved_publication_cf_list,
      $drupal_sync_field_list,
      'publication_cf'
      );
      $configFactory->set('desk_net_publication_custom_fields', serialize($new_publication_cf_list))->save();

      return TRUE;
    }

    return FALSE;
  }

}
