<?php

namespace Drupal\desk_net\Collection;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a collection of translated notice messages for the Desk-Net module.
 */
class NoticesCollection {

  /**
   * Getting notice message.
   *
   * @param int $code
   *   The code message.
   *
   * @return string
   *   The message text.
   */
  public static function getNotice(int $code): string {
    // Generate custom links.
    $dn_credentials_link = Link::fromTextAndUrl(\Drupal::translation()
      ->translate('Desk-Net Credentials'), Url::fromUri("internal:/admin/config/desk-net/desk-net-credentials"))
      ->toString();
    $support_link = Link::fromTextAndUrl('support@desk-net.com', Url::fromUri("mailto:support@desk-net.com"))
      ->toString();

    $message_list = [
      1 => \Drupal::translation()
        ->translate('Story update successfully sent to Desk-Net'),
      2 => \Drupal::translation()
        ->translate('Cannot update story in Desk-Net. The Drupal module could not find a
     corresponding story ID in Desk-Net. Code: 01'),
      3 => \Drupal::translation()
        ->translate('Cannot update story in Desk-Net. There is no corresponding story in
     Desk-Net. Code: 02'),
      4 => \Drupal::translation()
        ->translate("Cannot update the story in Desk-Net. Reason unknown. Please contact
     Desk-Net support at @link. Code: 03", ['@link' => $support_link]),
      5 => \Drupal::translation()
        ->translate("The Desk-Net API login credentials are not valid or have not been 
     entered. Please check the settings on the page @link in the Desk-Net module. Code: 04",
          ['@link' => $dn_credentials_link]),
      6 => \Drupal::translation()
        ->translate('Cannot create story in Desk-Net. Reason unknown. Code: 05'),
      7 => \Drupal::translation()
        ->translate('Connection successfully established.'),
      8 => \Drupal::translation()
        ->translate('Connection could not be established.'),
      9 => \Drupal::translation()
        ->translate('There is no connection from Drupal to Desk-Net. Code: 07'),
      10 => \Drupal::translation()
        ->translate('There is no connection from Desk-Net to Drupal. Code: 08'),
      11 => \Drupal::translation()
        ->translate("Cannot create story in Desk-Net. Publication date doesn't match platform schedule."),
      12 => \Drupal::translation()
        ->translate("Cannot update story in Desk-Net. Publication date doesn't match platform schedule."),
      13 => \Drupal::translation()
        ->translate('The configuration options have been saved.'),
    ];

    return $message_list[$code];
  }

}
