<?php

namespace Drupal\desk_net\EventSubscriber;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ActionController;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\DeleteMethods;
use Drupal\desk_net\Event\EntityUpdateEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to Desk-Net events to perform specific actions.
 *
 * This class implements the EventSubscriberInterface to listen for Desk-Net
 * events and trigger corresponding actions.
 */
class DeskNetSubscriber implements EventSubscriberInterface {

  /**
   * Manages entity types and their instances.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The action controller.
   *
   * @var \Drupal\desk_net\Controller\ActionController
   */
  protected $actionController;

  /**
   * Constructs a DeskNetSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\desk_net\Controller\ActionController $action_controller
   *   The action controller.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, DeleteMethods $delete_methods, ActionController $action_controller) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->deleteMethods = $delete_methods;
    $this->actionController = $action_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager'),
    $container->get('messenger'),
    $container->get('desk_net.delete_methods'),
    $container->get('desk_net.action_controller')
    );
  }

  /**
   * Defines the events to which the DeskNetSubscriber class subscribes.
   *
   * @return array
   *   An array where keys represent Desk-Net event names and values represent
   *   the method names in the DeskNetSubscriber class that handle those events.
   */
  public static function getSubscribedEvents() {
    return [
      EntityUpdateEvent::EVENT_NAME => 'processApiRequest',
    ];
  }

  /**
   * Builds and executes API requests for the given entity and action.
   *
   * @param \Drupal\desk_net\Event\EntityUpdateEvent $event
   *   The entity for which the API request is being processed.
   */
  public function processApiRequest(EntityUpdateEvent $event) {
    $entity = $event->entity;
    $action = $event->action;
    // Getting Desk-Net revision data.
    $desk_net_revision = ModuleSettings::deskNetRevisionGet($entity);

    switch ($action) {
      case 'insert':
        if (ModuleSettings::checkUserCredentials() && empty($desk_net_revision)) {
          $this->actionController->createNodeDrupalToDn($entity);
        }
        else {
          $this->messenger->addMessage([
            '#markup' => NoticesCollection::getNotice(5),
            '#allowed_tags' => ['a'],
          ], 'error');
        }
        break;

      case 'update':
        if (ModuleSettings::checkUserCredentials()) {
          if ($desk_net_revision !== FALSE && !empty($desk_net_revision['desk_net_story_id'])) {
            $this->actionController->updateNodeDrupalToDn($entity, $entity->bundle());
          }
          else {
            $this->actionController->createNodeDrupalToDn($entity);
          }
        }
        else {
          $this->messenger->addError();
          $this->messenger->addMessage([
            '#markup' => NoticesCollection::getNotice(5),
            '#allowed_tags' => ['a'],
          ], 'error');
        }
        break;

      case 'delete':
        try {
          $new_entity = $this->entityTypeManager
            ->getStorage($entity->getEntityTypeId())
            ->load($entity->id());
          if (!$new_entity) {
            $this->deleteMethods->deleteElement($entity);
          }
        }
        catch (PluginNotFoundException $e) {
          $this->deleteMethods->deleteElement($entity);
        }
    }
  }

}
