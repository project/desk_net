<?php

namespace Drupal\desk_net\PageTemplate;

use Drupal\desk_net\Controller\ActionController;

/**
 * Provides methods for generating page templates for Desk-Net matching pages.
 */
class PageTemplate {

  /**
   * Perform generation Template for Matching page.
   *
   * The array structure
   *  array array[key]
   *   The key has ID or Slug value
   *   string array[key][name]
   *   The element Name
   *   string array[key][parent]
   *   The element Parent.
   *
   * @param string $setting_name
   *   The Desk-Net setting name.
   * @param array $element_name_list
   *   The main elements list for the first block.
   * @param array $element_value_list
   *   The main elements list for the second block.
   * @param string $type
   *   The type element.
   * @param string $send_direction
   *   The direction for send elements.
   * @param string $default_value
   *   The default value.
   *
   * @return array
   *   The html code page template.
   */
  public static function generateDeskNetMatchingPageTemplate($setting_name, array $element_name_list, array $element_value_list, $type = '', $send_direction = '', $default_value = '') {
    foreach ($element_name_list as $element_name) {
      $title = '';
      $field_name = ActionController::generateFieldMatchingName($type, $send_direction, $element_name['id']);
      $field_value = \Drupal::config('desk_net.settings')->get($setting_name);

      if ($field_value !== NULL) {
        $field_value = unserialize($field_value, ['allowed_classes' => FALSE]);
      }

      if (!empty($field_value[$field_name])) {
        $selected_value = $field_value[$field_name];
      }
      else {
        $selected_value = $default_value;
      }

      if (isset($element_name['parent'])) {
        $parent_key = $element_name['parent'];
        foreach ($element_name_list as $element_key => $drupal_element) {
          if ($element_key == $parent_key) {
            $title .= $drupal_element['name'] . '<strong> - </strong>';
            break;
          }
        }
      }

      $title .= $element_name['name'];

      // Get parent for sub items.
      foreach ($element_value_list as $value) {
        if (isset($value['parent'])) {
          $parent_id = $value['parent'];
          foreach ($element_value_list as $drupal_element) {
            if ($drupal_element['id'] == $parent_id) {
              $parent_name = $drupal_element['name'];
              break;
            }
          }
        }

        if (!empty($parent_name)) {
          $element_value = $parent_name . ' - ' . $value['name'];
          $parent_name = '';
        }
        else {
          $element_value = $value['name'];
        }
        $option_list[$value['id']] = $element_value;
      }

      $form[$field_name] = [
        '#type' => 'select',
        '#title' => $title,
        '#options' => $option_list,
        '#default_value' => $selected_value,
        '#required' => FALSE,
      ];
    }

    return $form;
  }

}
