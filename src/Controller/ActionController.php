<?php

namespace Drupal\desk_net\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\DeleteMethods;
use Drupal\node\Entity\Node;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\token\TokenEntityMapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides actions related to Desk-Net integration.
 */
class ActionController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The alias manager service.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The token entity mapper service.
   *
   * @var \Drupal\token\TokenEntityMapperInterface
   */
  protected $tokenEntityMapper;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ActionController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   The alias manager service.
   * @param \Drupal\token\TokenEntityMapperInterface $token_entity_mapper
   *   The token entity mapper service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, DeleteMethods $delete_methods, LanguageManagerInterface $language_manager, AliasManagerInterface $alias_manager, TokenEntityMapperInterface $token_entity_mapper, AccountProxyInterface $current_user, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->deleteMethods = $delete_methods;
    $this->languageManager = $language_manager;
    $this->aliasManager = $alias_manager;
    $this->tokenEntityMapper = $token_entity_mapper;
    $this->currentUser = $current_user;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Sets the RequestsController instance.
   *
   * This method is used to inject the RequestsController dependency into this
   * ActionController instance. This approach, known as setter injection,
   * helps to resolve circular dependencies by setting the dependency after
   * the object has been instantiated.
   *
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The RequestsController instance to be set.
   */
  public function setRequestsController(RequestsController $requests_controller) {
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('entity_type.manager'),
    $container->get('desk_net.delete_methods'),
    $container->get('language_manager'),
    $container->get('path_alias.manager'),
    $container->get('token.entity_mapper'),
    $container->get('current_user'),
    $container->get('logger.factory')
    );

    $instance->setRequestsController($container->get('desk_net.requests_controller'));
    return $instance;
  }

  /**
   * Perform send node params to Desk-Net after insert node.
   *
   * @param object $entity
   *   The node data.
   */
  public function createNodeDrupalToDn($entity) {
    $response = $this->sendNodeParams($entity);

    switch ($response) {
      case 'do_not_import':
      case 'not_show_new_notice':
        break;

      case 'unauthorized':
        $this->messenger->addMessage([
          '#markup' => NoticesCollection::getNotice(5),
          '#allowed_tags' => ['a'],
        ], 'error');
        break;

      case FALSE:
        $this->messenger->addError(NoticesCollection::getNotice(6));
        break;

      case 'platform_schedule':
        $this->messenger->addError(NoticesCollection::getNotice(11));
        break;

      default:
        // Default Desk-Net revision data.
        $desk_net_revision = [
          'desk_net_story_id'              => NULL,
          'desk_net_status_name'           => NULL,
          'desk_net_publications_id'       => NULL,
          'desk_net_removed_status'        => NULL,
          'desk_net_description'           => NULL,
          'desk_net_content_change_status' => NULL,
          'desk_net_category_id'           => NULL,
          'desk_net_status_id'             => NULL,
        ];
        // Updating Desk-Net revision fields.
        $desk_net_revision['desk_net_story_id']        = $response['id'];
        $short_description                             = $this->getShortTitle($response['title']);
        $desk_net_revision['desk_net_description']     = html_entity_decode(mb_convert_encoding($short_description, 'UTF-8'));
        $desk_net_revision['desk_net_publications_id'] = $response['publications'][0]['id'];

        if (!empty($response['publications'][0]['status'])) {
          $desk_net_revision['desk_net_status_id']   = $response['publications'][0]['status'];
          $desk_net_revision['desk_net_status_name'] = $this->getStatusNameById($response['publications'][0]['status']);
        }
        else {
          $no_status_id = 1;

          $desk_net_revision['desk_net_status_id']   = $no_status_id;
          $desk_net_revision['desk_net_status_name'] = $this->getStatusNameById($no_status_id);
        }

        // Checking category on exist.
        if (isset($response['publications'][0]['category'])) {
          $desk_net_revision['desk_net_category_id'] = $response['publications'][0]['category'];
        }
        else {
          $desk_net_revision['desk_net_category_id'] = 'no_category';
        }

        // Update fields.
        $entity = ModuleSettings::deskNetRevisionSet($entity, $desk_net_revision);
        $entity->save(FALSE);

        $this->messenger->addStatus(NoticesCollection::getNotice(1));
    }
  }

  /**
   * Perform send node params to Desk-Net.
   *
   * @param object $entity
   *   The node data.
   *
   * @return array|bool
   *   The result of sending node params to Desk-Net.
   */
  private function sendNodeParams($entity) {
    $config = $this->configFactory->get('desk_net.settings');
    $platform_id = $config->get('platform_id');
    // Loading node by id.
    $entity = $this->entityTypeManager->getStorage('node')->load($entity->id());

    if (!empty($platform_id) && !empty(ModuleSettings::variableGet('desk_net_token'))) {
      $entity_data['title'] = $entity->title->value;
      // Updating Slug value.
      $entity_data = $this->sendSlugByCms($entity, $entity_data);

      $publication_position = 0;
      $entity_id = $entity->id();
      // Updating publication type.
      $field_name = $this->generateFieldMatchingName('type', 'drupal_to_desk_net', $entity->bundle());
      $content_type_matching = $this->getFieldMatchingValue('desk_net_selected_content_type_matching_list', $field_name);

      if ($content_type_matching !== NULL && $content_type_matching !== 'no_type' && $content_type_matching !== 'do_not_import') {
        $entity_data['publications'][$publication_position]['type'] = (int) $content_type_matching;
      }

      $cms_links = $this->getCmsLinks($entity_id);

      $entity_data['publications'][$publication_position]['assignments'] = [TRUE];
      $entity_data['publications'][$publication_position]['url_to_published_content'] = $cms_links['cmsOpenLink'];
      if (!empty($cms_links['cmsOpenLinkTitle'])) {
        $entity_data['publications'][$publication_position]['url_to_published_content_title'] = $cms_links['cmsOpenLinkTitle'];
      }

      $entity_data['publications'][$publication_position]['url_to_content_in_cms'] = $cms_links['cmsEditLink'];
      if (!empty($cms_links['cmsEditLinkTitle'])) {
        $entity_data['publications'][$publication_position]['url_to_content_in_cms_title'] = $cms_links['cmsEditLinkTitle'];
      }

      $entity_data['publications'][$publication_position]['platform'] = $platform_id;

      // Setting author.
      $user = $this->entityTypeManager->getStorage('user')->load($entity->getOwnerId());

      // Getting hash field name.
      $hash_author_field_name = ModuleSettings::variableGet('desk_net_author_id');

      if (!empty($user->mail->value) && !empty($user->name->value)) {
        if (isset($user->get($hash_author_field_name)->value) && !empty($user->get($hash_author_field_name)->value)) {
          $entity_data['tasks'][0]['user'] = intval($user->get($hash_author_field_name)->value);
        }
        else {
          $entity_data['tasks'][0]['user']['name'] = $user->name->value;
          $entity_data['tasks'][0]['user']['email'] = $user->mail->value;
        }
      }
      // Getting Desk-Net Task value.
      $field_name = $this->generateFieldMatchingName('task', NULL, 'default_task_in_desk_net');
      $desk_net_task_format = $this->getFieldMatchingValue('desk_net_selected_task_matching_list', $field_name);

      $entity_data['tasks'][0]['format'] = !$desk_net_task_format ? 18 : $desk_net_task_format;
      $entity_data['tasks'][0]['confirmationStatus'] = -2;

      // Getting Desk-Net Group value.
      $field_name = $this->generateFieldMatchingName('group', NULL, 'default_group_in_desk_net');
      $desk_net_group_value = $this->getFieldMatchingValue('desk_net_selected_group_matching_list', $field_name);

      if ($desk_net_group_value && $desk_net_group_value !== 'no_sync') {
        $entity_data['groups'][0] = $desk_net_group_value;
      }

      $entity_data['publications'][$publication_position]['cms_id'] = $entity_id;
      // Getting Desk-Net Status.
      $field_name = $this->generateFieldMatchingName('status', 'drupal_to_desk_net', $entity->status->value);
      $status_id = $this->getFieldMatchingValue('desk_net_selected_status_matching_list', $field_name);

      if (!empty($status_id) && $status_id !== 0 && $status_id !== 'no_sync') {
        $entity_data['publications'][$publication_position]['status'] = (int) $status_id;
      }
      else {
        $entity_data['publications'][$publication_position]['status'] = 1;
      }

      $entity_data['tasks'][0]['status'] = 1;

      // Selecting active Category ID.
      if ($entity->__isset('field_tags')) {
        $entity_data = $this->settingCategoryDrupalToDeskNet($entity, $entity_data, $publication_position);
      }
      // Checking on matching "Do not import".
      if ('do_not_import' === $entity_data || 'do_not_import' === $content_type_matching) {
        return 'do_not_import';
      }

      $drupal_publication_cf = ['title', 'url_alias'];

      foreach ($drupal_publication_cf as $publication_cf) {
        $field_name = $this->generateFieldMatchingName('publication_cf', 'drupal_to_desk_net', $publication_cf);
        $matching_publication_cf = $this->getFieldMatchingValue('desk_net_selected_publication_custom_field_matching_list', $field_name);

        if (empty($matching_publication_cf) || $matching_publication_cf === 'no_publication_cf') {
          continue;
        }

        $entity_data = $this->setPublicationCustomFieldToStory($entity, $publication_cf, $entity_data, $matching_publication_cf, $publication_position);
      }

      // Creating element on Desk-Net.
      $addition_post_info_from_desk_net = $this->requestsController->customRequest('POST', $entity_data, ModuleSettings::DN_BASE_URL, 'elements');

      // Clearing double notification message.
      switch ($addition_post_info_from_desk_net) {
        case 'unauthorized':
          return 'unauthorized';

        case 'not_show_new_notice':
          return 'not_show_new_notice';

        case 'platform_schedule':
          return 'platform_schedule';

        case FALSE:
          return FALSE;
      }

      $addition_post_info_from_desk_net = json_decode($addition_post_info_from_desk_net, TRUE);

      if (!empty($addition_post_info_from_desk_net['message'])) {
        return FALSE;
      }
      // Get Name Desk-Net status.
      $addition_post_info_from_desk_net['publication']['status']['name'] = $this->getStatusNameById($status_id);

      return $addition_post_info_from_desk_net;
    }
  }

  /**
   * Updating JSON Status Matching data.
   *
   * @param object $entity
   *   The node object.
   * @param array $entity_data
   *   The new post data.
   * @param int $publication_position
   *   The publication position number.
   *
   * @return array|string
   *   The new data with updated category id.
   */
  public static function settingCategoryDrupalToDeskNet($entity, array $entity_data, int $publication_position) {
    // Getting all entity tags.
    $select_list_category = $entity->field_tags->referencedEntities();

    if (!empty($select_list_category)) {
      // Clear category value.
      $category_value = 'do_not_import';
      // Scan category mapping WP to Desk-Net.
      foreach ($select_list_category as $category) {
        $field_name = ActionController::generateFieldMatchingName(
        'category', 'drupal_to_desk_net', $category->tid->value);

        $matching_value = ActionController::getFieldMatchingValue(
        'desk_net_selected_category_matching_list', $field_name);

        // Checking category mapping on exist value.
        if (empty($matching_value) && $category_value === 'do_not_import') {
          $category_value = 'no_category';
          continue;
        }

        // Skipping category 'do_not_import' & 'No category'.
        if ($category_value !== 'do_not_import' && $category_value !== 'no_category') {
          break;
        }
        elseif (!empty($matching_value) && $matching_value != 'do_not_import') {
          $category_value = $matching_value;
        }
      }
      // Set Desk-Net category.
      switch ($category_value) {
        case 'do_not_import':
          return 'do_not_import';

        case 'no_category':
          unset($entity_data['publications'][$publication_position]['category']);
          $entity_data['publications'][$publication_position]['platform'] = \Drupal::config('desk_net.settings')
            ->get('platform_id');
          break;

        default:
          unset($entity_data['publications'][$publication_position]['platform']);
          $entity_data['publications'][$publication_position]['category'] = (int) $category_value;
      }
    }
    else {
      // Default mapping on 'No category'.
      unset($entity_data['publications'][$publication_position]['category']);
      $entity_data['publications'][$publication_position]['platform'] = \Drupal::config('desk_net.settings')
        ->get('platform_id');
    }

    return $entity_data;
  }

  /**
   * Perform get Desk-Net status name by ID.
   *
   * @param int $status_id
   *   The Desk-Net Status ID.
   *
   * @return string
   *   The result of getting Desk-Net status name by ID.
   */
  private static function getStatusNameById($status_id) {
    $config            = \Drupal::config('desk_net.settings');
    $keys_status_field = [
      'desk_net_activate_status_list',
      'desk_net_deactivate_status_list',
    ];

    foreach ($keys_status_field as $value) {
      $status_list = $config->get($value);

      if ($status_list !== NULL) {
        $status_list = unserialize($status_list, ['allowed_classes' => FALSE]);

        foreach ($status_list as $status) {
          if ($status['id'] == $status_id) {
            return $status['name'];
          }
        }
      }
    }

    return \Drupal::translation()->translate('No Status');
  }

  /**
   * Perform sending update node data to Desk-Net.
   *
   * @param object $entity
   *   The node data.
   * @param string $content_type
   *   The element content type.
   *
   * @return bool
   *   The result of updating story in Desk-Net.
   */
  public function updateNodeDrupalToDn($entity, $content_type) {
    // Getting Desk-Net revision data.
    $desk_net_revision = ModuleSettings::deskNetRevisionGet($entity);
    // Checking element on status "Delete".
    if (!empty($desk_net_revision['desk_net_removed_status']) && $desk_net_revision['desk_net_removed_status'] == 'desk_net_removed'
    ) {
      return FALSE;
    }

    if ($entity->id()) {
      // Get JSON with default value.
      $story_data = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'elements', $desk_net_revision['desk_net_story_id']);

      if ($story_data === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(3));

        return FALSE;
      }

      if (is_string($story_data)) {
        $story_data = json_decode($story_data, TRUE);
      }

      if ($story_data === 'not_show_new_notice' || empty($story_data)
                 || !empty($story_data['message'])
                 || empty($story_data['publications'])
      ) {
        if ($story_data !== FALSE) {
          $this->messenger->addError(NoticesCollection::getNotice(3));
        }
        else {
          $this->messenger->addMessage([
            '#markup' => NoticesCollection::getNotice(4),
            '#allowed_tags' => ['a'],
          ], 'error');
        }

        return FALSE;
      }
      // Update story data before send to Desk-Net.
      $new_story_data = ModuleSettings::updateDataBeforeSendToDn($story_data, $entity, $content_type);
      // Update with error.
      if ($new_story_data == 'do_not_import') {
        return FALSE;
      }

      // Sending request to Desk-Net for update story.
      $response = $this->requestsController->customRequest('PUT', $new_story_data,
      ModuleSettings::DN_BASE_URL, 'elements', $new_story_data['id']);

      // Check request status on error message.
      switch ($response) {
        case 'platform_schedule':
          $this->messenger->addError(NoticesCollection::getNotice(12));

          return FALSE;

        case 'unauthorized':
          $this->messenger->addError(NoticesCollection::getNotice(3));

          return FALSE;

        default:
          if (empty($response)) {
            $this->messenger->addError(NoticesCollection::getNotice(3));

            return FALSE;
          }

          $response_body = json_decode($response, TRUE);

          if (!empty($response_body['message'])) {
            $this->messenger->addError(NoticesCollection::getNotice(3));
            $this->loggerFactory->get('desk_net')->error('Failed to update Story with ID {id}: {message}', [
              'id' => $new_story_data['id'],
              'message' => $response_body['message'],
            ]);

            return FALSE;
          }

          if ($response !== 'not_show_new_notice') {
            $this->messenger->addStatus(NoticesCollection::getNotice(1));
          }
      }
      // Updating Desk-Net status revision.
      $publication_id       = $desk_net_revision['desk_net_publications_id'] ?? NULL;
      $publication_position = ModuleSettings::getPublicationPosition(
      $new_story_data['publications'], $publication_id);
      $response_body        = json_decode($response, TRUE);

      if (isset($response_body['publications'][$publication_position]['status'])) {
        $desk_net_status_id = $new_story_data['publications'][$publication_position]['status'];

        $desk_net_revision['desk_net_status_id']   = $response_body['publications'][$publication_position]['status'];
        $desk_net_revision['desk_net_status_name'] = self::getStatusNameById($desk_net_status_id);
      }
      else {
        $no_status_id = 1;

        $desk_net_revision['desk_net_status_id']   = $no_status_id;
        $desk_net_revision['desk_net_status_name'] = self::getStatusNameById($no_status_id);
      }
      // Updating Desk-Net category revision.
      if (!empty($response_body['publications'][$publication_position]['category'])) {
        $desk_net_revision['desk_net_category_id'] = $response_body['publications'][$publication_position]['category'];
      }
      else {
        $desk_net_revision['desk_net_category_id'] = 'no_category';
      }

      $entity = ModuleSettings::deskNetRevisionSet($entity, $desk_net_revision);

      $entity->save(FALSE);
    }
  }

  /**
   * Perform edit article data.
   *
   * @param array $data
   *   The story data.
   *
   * @return array|bool
   *   The result creating article in Drupal.
   */
  public function createNode(array $data) {
    // Getting current user.
    $user = $this->currentUser;

    // Setting value 'No type' if data from Desk-Net coming without type_id.
    if (empty($data['publication']['type']['id'])) {
      $data['publication']['type']['id'] = 'no_type';
    }
    // Getting value form content-type matching.
    $field_name   = self::generateFieldMatchingName(
    'type', 'desk_net_to_drupal', $data['publication']['type']['id']);
    $content_type = self::getFieldMatchingValue(
    'desk_net_selected_content_type_matching_list', $field_name);

    if ($content_type == NULL) {
      $content_type = 'article';
    }
    elseif ('do_not_import' === $content_type) {
      return FALSE;
    }

    $node = Node::create(['type' => $content_type]);

    if (empty($data['description'])) {
      return FALSE;
    }
    // Adding creator.
    if (is_object($user) && isset($user->uid->value)) {
      $node->set('uid', $user->uid->value);
    }

    // Set author if it exists.
    if (!empty($data['tasks'])) {
      foreach ($data['tasks'] as $entity) {
        if (!empty($entity['format']['name']) && !empty($entity['assignee'])) {
          $user_id = self::validUser($entity['assignee']['name'],
          $entity['assignee']['id']);
          if ($user_id !== FALSE) {
            $node->set('uid', $user_id);
            break;
          }
        }
      }
    }

    // Updating Status.
    if (isset($data['publication']['status']['id'])) {
      $field_name             = self::generateFieldMatchingName(
      'status', 'desk_net_to_drupal', $data['publication']['status']['id']);
      $drupal_status_matching = self::getFieldMatchingValue(
      'desk_net_selected_status_matching_list', $field_name);

      if ($drupal_status_matching !== NULL) {
        $node->set('status', $drupal_status_matching);
      }
      else {
        $node->set('status', 0);
      }
    }

    // Getting entity title.
    $title       = self::getTitleFromDeskNetApp($data, 'create_post');
    $short_title = self::getShortTitle($title);
    $node->set('title', $short_title);

    if (!empty($data['slug']) && 'url_alias' === self::getSlugSetting()) {
      $node = self::setUrlAlias($node, $data['slug']);
    }

    if (!empty($data['publication']['customFields'])) {
      foreach ($data['publication']['customFields'] as $customField) {
        $field_name              = self::generateFieldMatchingName(
        'publication_cf', 'desk_net_to_drupal', $customField['customFieldId']);
        $matching_publication_cf = self::getFieldMatchingValue(
        'desk_net_selected_publication_custom_field_matching_list', $field_name);
        $matching_value          = $customField['value'] ?? NULL;
        $node                    = self::setPublicationCustomFieldToNode($node, $matching_publication_cf, $matching_value);
      }
    }

    // The default category value.
    $category_id = 'no_category';

    // Update field tags in Drupal 9.
    if ($node->__isset('field_tags')) {
      if (!empty($data['publication']['category']['id'])) {
        $category_id = $data['publication']['category']['id'];
      }

      $field_name = self::generateFieldMatchingName(
      'category', 'desk_net_to_drupal', $category_id);
      $field_value = self::getFieldMatchingValue(
      'desk_net_selected_category_matching_list', $field_name);

      if ($field_value == 'do_not_import') {
        return FALSE;
      }
      else {
        if ($field_value && $field_value != 'no_category') {
          $node->set('field_tags', [$field_value]);
        }
      }
    }

    try {
      $short_description = self::getShortTitle($data['description']);
      // Set Desk-Net revision data.
      $desk_net_revision = [
        'desk_net_story_id'              => $data['id'],
        'desk_net_status_name'           => $data['publication']['status']['name'],
        'desk_net_publications_id'       => $data['publication']['id'],
        'desk_net_removed_status'        => NULL,
        'desk_net_description'           => preg_replace('/[\r\n\t ]+/', ' ', $short_description),
        'desk_net_content_change_status' => NULL,
        'desk_net_category_id'           => $category_id,
        'desk_net_status_id'             => $data['publication']['status']['id'],
      ];

      $node = ModuleSettings::deskNetRevisionSet($node, $desk_net_revision);
      // Bad updating Desk-Net revision.
      if ($node === FALSE) {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('Not_found_Desk-Net_custom_fields')
        ->warning($e->getMessage());
    }

    try {
      $node->save();
      $this->loggerFactory->get('desk_net')->info('Article with ID {id} has been created by Desk-Net API.', [
        'id' => $node->id(),
      ]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('desk_net')->error('Failed to create article: {message}', [
        'message' => $e->getMessage(),
      ]);
    }

    $nid = $node->id();

    return $this->generateDrupalResponseData($nid);
  }

  /**
   * Perform update node in Drupal by Desk-Net.
   *
   * @param array $data
   *   The node data.
   * @param int $node_id
   *   The node id for update.
   * @param array $desk_net_revision
   *   The Desk-Net revision data.
   *
   * @return array|bool
   *   The result updating article in Drupal from Desk-Net.
   */
  public function updateNode(array $data, int $node_id, $desk_net_revision) {
    // Setting value 'No type' if data from Desk-Net coming without type_id.
    if (empty($data['publication']['type']['id'])) {
      $data['publication']['type']['id'] = 'no_type';
    }
    // Getting value form content-type matching.
    $field_name   = self::generateFieldMatchingName(
    'type', 'desk_net_to_drupal', $data['publication']['type']['id']);
    $content_type = self::getFieldMatchingValue(
    'desk_net_selected_content_type_matching_list', $field_name);

    if ('do_not_import' === $content_type) {
      return FALSE;
    }

    // Check Category mapping in Drupal.
    $field_value = null;

    if (!empty($data['publication']['category']['id'])) {
      $field_name = self::generateFieldMatchingName(
      'category', 'desk_net_to_drupal', $data['publication']['category']['id']);
      $field_value = self::getFieldMatchingValue(
      'desk_net_selected_category_matching_list', $field_name);
    }

    $no_category_field_name  = self::generateFieldMatchingName(
    'category', 'desk_net_to_drupal', 'no_category');
    $no_category_field_value = self::getFieldMatchingValue(
    'desk_net_selected_category_matching_list', $no_category_field_name);

    if ($field_value === 'do_not_import'
               || (!isset($data['publication']['category']['id']) && $no_category_field_value === 'do_not_import')
    ) {
      return FALSE;
    }

    // Loading node by id.
    $node = $this->entityTypeManager->getStorage('node')->load($node_id);

    // Updating node Status.
    $initial_status_syncing = (bool) $this->configFactory->get('desk_net.settings')->get('initial_status_syncing') ?? FALSE;

    if (isset($data['publication']['status']['id'])
             && $desk_net_revision['desk_net_status_id'] != $data['publication']['status']['id']
             && !$initial_status_syncing) {
      $field_name             = self::generateFieldMatchingName(
      'status', 'desk_net_to_drupal', $data['publication']['status']['id']);
      $drupal_status_matching = self::getFieldMatchingValue(
      'desk_net_selected_status_matching_list', $field_name);

      if ($drupal_status_matching !== NULL) {
        $node->set('status', $drupal_status_matching);
      }
      else {
        $node->set('status', 0);
      }
    }

    // Checking Desk-Net category on 'no category' value.
    if (!isset($data['publication']['category']['id'])) {
      $data['publication']['category']['id'] = 'no_category';
    }

    // Checking Tags fields.
    if ($node->__isset('field_tags') && $desk_net_revision['desk_net_category_id'] != $data['publication']['category']['id']) {
      // Delete old taxonomies value.
      if (!empty($node->field_tags->referencedEntities())) {
        unset($node->field_tags);
        $node->field_tags = [];
      }
      // Setting new tag for element in Drupal 9
      // using the Desk-Net Category Mapping.
      if (!empty($data['publication']['category']['id'])) {
        $field_name = self::generateFieldMatchingName(
          'category', 'desk_net_to_drupal', $data['publication']['category']['id']);
      } else {
        $field_name = self::generateFieldMatchingName(
          'category', 'desk_net_to_drupal', 'no_category');
      }

      $field_value = self::getFieldMatchingValue(
      'desk_net_selected_category_matching_list', $field_name);
      $field_value = ($field_value == 'no_category') ? NULL : $field_value;
      $node->set('field_tags', $field_value);

      // Updating Desk-Net revision Category id.
      $desk_net_revision['desk_net_category_id'] = $data['publication']['category']['id'];
    }

    // Updating Desk-Net description for Drupal element.
    if (!empty($data['description'])) {
      $short_description                         = self::getShortTitle($data['description']);
      $desk_net_revision['desk_net_description'] = $short_description;
    }
    // Set author if it exists.
    if (!empty($data['tasks'])) {
      foreach ($data['tasks'] as $entity) {
        if (!empty($entity['format']['name'])) {
          if (!empty($entity['assignee'])) {
            $user_id = self::validUser($entity['assignee']['name'],
            $entity['assignee']['id']);

            if ($user_id !== FALSE) {
              $node->set('uid', $user_id);
              break;
            }
          }
        }
      }
    }

    // Getting entity title.
    $title = self::getTitleFromDeskNetApp($data, 'update_post');

    if (NULL !== $title) {
      $short_title = self::getShortTitle($title);
      $node->set('title', $short_title);
    }

    $initial_slug_syncing = $this->configFactory->get('desk_net.settings')->get('initial_slug_syncing');

    if (!empty($data['slug']) && !$initial_slug_syncing && 'url_alias' === self::getSlugSetting()) {
      $node = self::setUrlAlias($node, $data['slug']);
    }

    if (!empty($data['publication']['customFields'])) {
      foreach ($data['publication']['customFields'] as $customField) {
        $field_name              = self::generateFieldMatchingName(
        'publication_cf', 'desk_net_to_drupal', $customField['customFieldId']);
        $matching_publication_cf = self::getFieldMatchingValue(
        'desk_net_selected_publication_custom_field_matching_list', $field_name);
        $matching_value          = $customField['value'] ?? NULL;

        $node = self::setPublicationCustomFieldToNode($node, $matching_publication_cf, $matching_value);
      }
    }

    // Updating Status field in block additional info.
    $desk_net_revision['desk_net_status_name'] = $data['publication']['status']['name'];
    // Updating Desk-Net revision Status id.
    $desk_net_revision['desk_net_status_id'] = $data['publication']['status']['id'];

    // Updating Desk-Net revision.
    $node = ModuleSettings::deskNetRevisionSet($node, $desk_net_revision);

    // Saving changed.
    try {
      $node->save();
      $this->loggerFactory->get('desk_net')->info('Article with ID {id} has been updated by Desk-Net API.', [
        'id' => $node->id(),
      ]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('desk_net')->error('Failed to update article: {message}', [
        'message' => $e->getMessage(),
      ]);
    }

    $nid = $node->id();

    return $this->generateDrupalResponseData($nid);
  }

  /**
   * Perform generating the response Desk-Net module data.
   *
   * @param int $nid
   *   The node id.
   *
   * @return array
   *   The response data.
   */
  public function generateDrupalResponseData(int $nid): array {
    $response_data = [
      'id' => $nid,
    ];

    $cms_links = self::getCmsLinks($nid);

    return $response_data + $cms_links;
  }

  /**
   * Getting the custom links title for Desk-Net CMS links.
   *
   * @param int $nid
   *   The node id.
   *
   * @return array
   *   An array containing the custom links for Desk-Net CMS, including
   *   the edit link, open link, and their respective titles, if configured.
   */
  public static function getCmsLinks(int $nid) {
    $response_data = [];

    $cmsEditLink = Url::fromRoute('entity.node.edit_form', ['node' => $nid]);
    $cmsEditLink->setAbsolute();
    $cmsOpenLink = Url::fromRoute('entity.node.canonical', ['node' => $nid]);
    $cmsOpenLink->setAbsolute();

    $config                         = \Drupal::config('desk_net.settings');
    $url_to_content_in_cms_title    = $config->get('url_to_content_in_cms_title');
    $url_to_published_content_title = $config->get('url_to_published_content_title');

    $response_data['cmsEditLink'] = $cmsEditLink->toString();
    $response_data['cmsOpenLink'] = $cmsOpenLink->toString();

    if ($url_to_content_in_cms_title) {
      $response_data['cmsEditLinkTitle'] = $url_to_content_in_cms_title;
    }

    if ($url_to_published_content_title) {
      $response_data['cmsOpenLinkTitle'] = $url_to_published_content_title;
    }

    return $response_data;
  }

  /**
   * Getting the title value from Desk-Net data.
   *
   * @param array $data_from_dn
   *   The Story data.
   * @param string $type_action
   *   The type action with Post element.
   *
   * @return string|null
   *   The title value extracted from the Desk-Net data, or NULL if not found.
   */
  public static function getTitleFromDeskNetApp(array $data_from_dn, $type_action = 'create_post') {
    $title = NULL;
    // Getting first value Headline field.
    $headline = self::getHeadlineField($data_from_dn);

    if (!empty($headline)) {
      return $headline;
    }
    else {
      $initial_slug_syncing = \Drupal::config('desk_net.settings')
        ->get('initial_slug_syncing');

      if (!empty($data_from_dn['slug'])
                 && 'title' === self::getSlugSetting()
                 && (!$initial_slug_syncing || 'create_post' === $type_action)) {
        return self::sanitizeStringFromDeskNet($data_from_dn['slug']);
      }
    }

    if ('create_post' === $type_action) {
      $title = self::sanitizeStringFromDeskNet($data_from_dn['description']);
    }

    return $title;
  }

  /**
   * Getting Headline field from Desk-Net data.
   *
   * @param array $data
   *   The Story data from Desk-Net.
   *
   * @return string|null
   *   The value of the Headline field, or NULL if not found.
   */
  public static function getHeadlineField($data) {
    if (!empty($data['tasks'])) {
      foreach ($data['tasks'] as $value) {
        if (!empty($value['headline'])) {
          return self::sanitizeStringFromDeskNet($value['headline']);
        }
      }
    }

    return NULL;
  }

  /**
   * Sanitize string from Desk-Net fields.
   *
   * @param string $string
   *   The sanitizing string.
   *
   * @return string
   *   Return sanitize string.
   */
  public static function sanitizeStringFromDeskNet($string) {
    $string = preg_replace('/[\r\n\t ]+/', ' ', $string);

    return $string;
  }

  /**
   * Sets a URL alias for a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node entity.
   * @param string $value
   *   The value to base the alias on.
   *
   * @return \Drupal\node\Entity\Node
   *   The updated node entity.
   */
  public static function setUrlAlias($node, $value) {
    $languageManager = \Drupal::languageManager();
    $languages = $languageManager->getLanguages();
    $path_alias_repository = \Drupal::service('path_alias.repository');
    $alias_cleaner = \Drupal::service('pathauto.alias_cleaner');
    $max_length = 255;

    foreach ($languages as $language) {
      if ($language->getId() == LanguageInterface::LANGCODE_NOT_SPECIFIED) {
        // Skip unspecified language.
        continue;
      }

      // Clearing string for use in URLs.
      $url_alias = $alias_cleaner->cleanString($value);

      // Truncate the alias if it exceeds the maximum length.
      if (strlen($url_alias) > $max_length) {
        $url_alias = substr($url_alias, 0, $max_length);
      }

      // Ensure the alias does not exceed the maximum length with the leading slash.
      if (strlen($url_alias) >= $max_length) {
        $url_alias = substr($url_alias, 0, $max_length - 1);
      }

      if (!$path_alias_repository->lookupByAlias('/' . $url_alias, $language->getId())) {
        // Updating Node alias and turn off automatic URL alias generation.
        $node->set('path', [
          'pathauto' => FALSE,
          'alias' => '/' . $url_alias,
        ]);
      }
    }

    return $node;
  }

  /**
   * Getting Slug Syncing settings.
   *
   * @return string
   *   Return Slug Syncing setting value.
   */
  public static function getSlugSetting() {
    // Getting Slug setting value.
    $slug_syncing = \Drupal::config('desk_net.settings')
      ->get('desk_net_slug_syncing');

    if ($slug_syncing !== NULL) {
      $slug_syncing = unserialize($slug_syncing, ['allowed_classes' => FALSE]);
    }

    $field_value = $slug_syncing['desk_net_slug_syncing'] ?? NULL;

    if (empty($field_value)) {
      $field_value = 'url_alias';
    }

    return $field_value;
  }

  /**
   * Truncates the node title in Drupal.
   *
   * @param string $title
   *   The title string to be truncated.
   *
   * @return string
   *   The truncated node title.
   */
  public static function getShortTitle($title) {
    $short_title = $title;

    if (!empty($title) && strlen($title) > 77) {
      $short_title = mb_substr($title, 0, 77) . '...';
    }

    return $short_title;
  }

  /**
   * Updates the slug in Desk-Net from the CMS.
   *
   * @param object $node
   *   The node object.
   * @param array $entity_data
   *   The story data.
   *
   * @return array
   *   The updated story data.
   */
  public static function sendSlugByCms($node, $entity_data) {
    $field_value = self::getSlugSetting();
    $langcode    = \Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();

    switch ($field_value) {
      case 'title':
        $entity_data['slug'] = $node->title->value;

        break;

      default:
        $current_path  = $node->toUrl()->toString();
        $current_alias = \Drupal::service('path_alias.manager')
          ->getAliasByPath($current_path, $langcode);

        if (!$node->path->pathauto || $current_alias !== $current_path) {
          $entity_data['slug'] = $current_alias ?: $current_path;
        }
        else {
          // Build token data.
          $data = [
            'node' => $node,
          ];

          $pattern = \Drupal::service('pathauto.generator')
            ->getPatternByEntity($node);

          $alias = \Drupal::token()
            ->replace($pattern->getPattern(), $data, [
              'clear'    => TRUE,
              'langcode' => $langcode,
              'pathauto' => TRUE,
            ], new BubbleableMetadata());

          $alias_substrings = explode('/', $alias);

          foreach ($alias_substrings as &$value) {
            $value = \Drupal::service('pathauto.alias_cleaner')
              ->cleanString($value);
          }

          $entity_data['slug'] = implode('/', $alias_substrings);
        }

        $entity_data['slug'] = ltrim($entity_data['slug'], '/');
    }

    return $entity_data;
  }

  /**
   * The generate string with publication Time.
   *
   * @param string $time
   *   The post time.
   * @param string $date
   *   The post date.
   *
   * @return string
   *   The publication time.
   */
  private static function generateTimeString($time, $date) {
    if (empty($time)) {
      $time = '23:59:00';
    }
    if (empty($date)) {
      $date = new \DateTime('now', new \DateTimeZone('UTC'));
      $date = $date->format('Y-m-d');
    }
    $date = new \DateTime($date . ' ' . $time, new \DateTimeZone('UTC'));

    return (string) $date->getTimestamp();
  }

  /**
   * Perform validate user from Desk-Net by email.
   *
   * @param string $email
   *   The email Desk-Net user.
   * @param int $author_id
   *   The Desk-Net user ID.
   *
   * @return bool|int
   *   The Drupal user id.
   */
  private static function validUser($email, $author_id) {
    $user = user_load_by_mail($email);

    // Getting hash field name.
    $hash_field_name = ModuleSettings::variableGet('desk_net_author_id');

    // If field name with Desk-Net revision data was not found.
    if ($hash_field_name === NULL) {
      return FALSE;
    }

    if ($user !== FALSE && $user->__isset($hash_field_name)) {
      if ($user->get($hash_field_name)->value === NULL) {
        $user->set($hash_field_name, $author_id);
        $user->save();
      }

      $user_uid = $user->uid->value;

      return $user_uid;
    }

    return FALSE;
  }

  /**
   * Perform get Category list from Desk-Net.
   *
   * @return array|false
   *   The result loading category list from Desk-Net, or FALSE on failure.
   */
  public function getCategory(EntityTypeManagerInterface $entityTypeManager) {
    // Get configuration variables.
    $platformId = $this->configFactory->get('desk_net.settings')
      ->get('platform_id');
    $token      = ModuleSettings::variableGet('desk_net_token');
    $baseUrl    = ModuleSettings::DN_BASE_URL;

    // Check if required variables are set.
    if (empty($platformId) || empty($token)) {
      return FALSE;
    }

    // Make the API request to get the category list.
    $categoryList = $this->requestsController->get($baseUrl, 'categories/platform', $platformId);

    if ($categoryList === 'unauthorized') {
      $this->messenger->addError(NoticesCollection::getNotice(3));

      return FALSE;
    }

    $categoryList = json_decode($categoryList, TRUE);

    // Check for errors in the API response.
    if (isset($categoryList['message']) || $categoryList === 'not_show_new_notice' || empty($categoryList)) {
      return FALSE;
    }

    // Save the category list for the platform.
    $this->configFactory
      ->getEditable('desk_net.settings')
      ->set('desk_net_category_list', serialize($categoryList))
      ->save();

    // Optionally, update Drupal taxonomy terms based on
    // the Desk-Net categories.
    $this->updateDrupalCategories($categoryList, $entityTypeManager);

    return $categoryList;
  }

  /**
   * Update Drupal taxonomy terms based on Desk-Net categories.
   *
   * @param array $categoryList
   *   The category list from Desk-Net.
   */
  private function updateDrupalCategories(array $categoryList) {
    $saveCategoryList = $this->configFactory->get('desk_net.settings')->get('desk_net_category_list');

    if ($saveCategoryList !== NULL) {
      $saveCategoryList = unserialize($saveCategoryList, ['allowed_classes' => FALSE]);
    }

    if (empty($saveCategoryList)) {
      return;
    }

    $elementListId = [];

    $vocabulary_storage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
    $vocabulary = $vocabulary_storage->loadMultiple();
    $tagsVocabulary = $vocabulary['tags'];

    if ($tagsVocabulary) {
      $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
      $vocabularyTermList = $termStorage->loadTree($tagsVocabulary->id());

      foreach ($vocabularyTermList as $term) {
        $elementListId[] = $term->tid;
      }
    }

    $this->deleteMethods->shapeDeletedItems('desk_net_selected_category_matching_list', $categoryList, $saveCategoryList, $elementListId, 'category');
  }

  /**
   * Validates if a given content type exists.
   *
   * @param string $content_type
   *   The machine name of the content type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface object.
   *
   * @return bool
   *   TRUE if the content type exists, FALSE otherwise.
   */
  public static function validateContentType(string $content_type, EntityTypeManagerInterface $entityTypeManager): bool {
    $nodeTypeStorage = $entityTypeManager->getStorage('node_type');
    if (isset($nodeTypeStorage)) {
      $nodeType = $nodeTypeStorage->load($content_type);

      return ($nodeType !== NULL);
    }

    return FALSE;
  }

  /**
   * Generates a field matching name based on type, send direction, and matching field name.
   *
   * This method generates a field matching name using the provided parameters:
   * - $type: The type of the field matching.
   * - $send_direction: The direction of the field matching.
   * - $matching_field_value: The value of the matching field.
   *
   * @param string|null $type
   *   The type of the field matching.
   * @param string|null $send_direction
   *   The direction of the field matching.
   * @param string|null $matching_field_value
   *   The value of the matching field.
   *
   * @return string
   *   The generated field matching name.
   */
  public static function generateFieldMatchingName($type = NULL, $send_direction = NULL, $matching_field_value = NULL) {
    $field_name = 'desk_net';
    $field_name = $type ? $field_name . '_' . $type : $field_name;
    $field_name = $send_direction ? $field_name . '_' . $send_direction : $field_name;

    return $field_name . '_' . $matching_field_value;
  }

  /**
   * Retrieves the value of a field matching setting.
   *
   * @param string $setting_name
   *   The name of the setting in the configuration.
   * @param string $field_name
   *   The name of the field.
   * @param string|null $default_name
   *   (Optional) The default value if the field value is not found.
   *
   * @return mixed
   *   The value of the field matching setting.
   */
  public static function getFieldMatchingValue($setting_name, $field_name, $default_name = NULL) {
    $field_value = \Drupal::config('desk_net.settings')
      ->get($setting_name);

    if ($field_value !== NULL) {
      $field_value = unserialize($field_value, ['allowed_classes' => FALSE]);
    }

    if (!empty($field_value[$field_name])) {
      $selected_value = $field_value[$field_name];
    }
    else {
      $selected_value = $default_name;
    }

    return $selected_value;
  }

  /**
   * Sets the value of a field matching setting.
   *
   * @param string $setting_name
   *   The name of the setting in the configuration.
   * @param string $field_name
   *   The name of the field.
   * @param mixed $new_value
   *   The new value to set for the field.
   */
  public function setFieldMatchingValue($setting_name, $field_name, $new_value) {
    $field_value = $this->configFactory->get('desk_net.settings')->get($setting_name);

    if ($field_value !== NULL) {
      $field_value = unserialize($field_value, ['allowed_classes' => FALSE]);
    }

    $field_value[$field_name] = $new_value;

    $this->configFactory
      ->getEditable('desk_net.settings')
      ->set($setting_name, serialize($field_value))
      ->save();
  }

  /**
   * Sets a custom field value to the provided entity based on the specified matching criteria.
   *
   * This method sets a custom field value to the given entity based on the matching criteria provided.
   *
   * The supported matching criteria include 'title' and 'url_alias'.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to which the custom field value will be set.
   * @param string $matching
   *   The matching criteria indicating which field to set. Supported values: 'title', 'url_alias'.
   * @param mixed $value
   *   The value to set for the custom field.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The modified entity with the custom field value set.
   */
  public static function setPublicationCustomFieldToNode($entity, $matching, $value) {
    switch ($matching) {
      case 'title':
        $short_title = self::getShortTitle($value);
        $entity->set('title', $short_title);
        break;

      case 'url_alias':
        $entity = self::setUrlAlias($entity, $value);
        break;
    }

    return $entity;
  }

  /**
   * Sets a custom field value to the specified story data based on the provided matching criteria.
   *
   * This method sets a custom field value to the given story data based on the provided matching criteria.
   *
   * The supported matching criteria include 'title' and 'url_alias'.
   *
   * @param mixed $entity
   *   The entity corresponding to the story data.
   * @param string $matching
   *   The matching criteria indicating which field to set. Supported values: 'title', 'url_alias'.
   * @param array $story_data
   *   The story data to which the custom field value will be set.
   * @param string $matching_publication_cf
   *   The identifier of the matching publication custom field.
   * @param int $publication_position
   *   (optional) The position of the publication in the story data. Defaults to 0.
   *
   * @return array
   *   The modified story data with the custom field value set.
   */
  public static function setPublicationCustomFieldToStory(
    $entity,
    $matching,
    $story_data,
    $matching_publication_cf,
    $publication_position = 0,
  ) {
    switch ($matching) {
      case 'title':
        $story_data['publications'][$publication_position]['customFields'][] = [
          'id'    => $matching_publication_cf,
          'value' => $entity->title->value,
        ];

        break;

      case 'url_alias':
        $url_alias = \Drupal::service('path_alias.manager')
          ->getAliasByPath('/node/' . $entity->id());

        $story_data['publications'][$publication_position]['customFields'][] = [
          'id'    => $matching_publication_cf,
          'value' => ltrim($url_alias, '/') ?? NULL,
        ];

        break;
    }

    return $story_data;
  }

}
