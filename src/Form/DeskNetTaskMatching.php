<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Desk-Net task matching settings.
 */
class DeskNetTaskMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetTaskMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_task_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration_setting_name = 'desk_net_selected_task_matching_list';
    $desk_net_task_list = $this->getTasks();

    $platform_id = $this->configFactory->get('desk_net.settings')->get('platform_id');

    if (!empty($platform_id)) {
      if ($desk_net_task_list && !empty(ModuleSettings::variableGet('desk_net_token'))) {
        $html = '<h2>' . $this->stringTranslation->translate('Tasks Matching') . '</h2>';
        $html .= '<p>';
        $html .= $this->stringTranslation->translate('Use this page to match the default task format in Desk-Net on creating a Story from Drupal to Desk-Net.');
        $html .= '</p>';
        $html .= '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

        $form['html'] = [
          '#markup' => $html,
        ];

        $desk_net_to_drupal = [
          'default_task_in_desk_net' => [
            'id' => 'default_task_in_desk_net',
            'name' => 'Default Task in Desk-Net',
          ],
        ];

        foreach ($desk_net_task_list as $task) {
          $drupal_to_desk_net[$task['id']]['id'] = $task['id'];
          $drupal_to_desk_net[$task['id']]['name'] = $task['name'];
        }

        $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
        'task', '', '18');

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->stringTranslation->translate('Save'),
        ];

        return $form;
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          $formValues[$key] = $value;
        }
      }

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_task_matching_list', serialize($formValues))
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Retrieves tasks from the Desk-Net platform.
   *
   * @return bool|array
   *   The result of loading the tasks list from Desk-Net, or FALSE if
   *   unsuccessful.
   */
  private function getTasks() {
    $config = $this->configFactory->get('desk_net.settings');
    $platform_id = $config->get('platform_id');
    $saved_task_list_for_platform = $config->get('desk_net_task_list');

    if ($saved_task_list_for_platform !== NULL) {
      $saved_task_list_for_platform = unserialize($saved_task_list_for_platform, ['allowed_classes' => FALSE]);
    }

    if (!empty($platform_id)) {
      $task_list_for_platform = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'formats');

      if ($task_list_for_platform === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(9));
        return $saved_task_list_for_platform;
      }

      $task_list_for_platform = json_decode($task_list_for_platform, TRUE);

      if (!empty($task_list_for_platform['message']) || $task_list_for_platform === 'not_show_new_notice' || empty($task_list_for_platform)) {
        return $saved_task_list_for_platform;
      }

      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_task_list', serialize($task_list_for_platform))
        ->save();

      return $task_list_for_platform;
    }

    return NULL;
  }

}
