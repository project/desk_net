<?php

namespace Drupal\desk_net\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for managing Drupal API credentials.
 *
 * This controller provides functionality for managing Drupal API credentials,
 * including validation and retrieval.
 */
class DrupalCredentialsController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The API key for authentication.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The API secret for authentication.
   *
   * @var string
   */
  protected $apiSecret;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a DrupalCredentialsController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, MessengerInterface $messenger, Connection $database, TranslationInterface $translation) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->database = $database;
    $this->stringTranslation = $translation;

    $config = $this->configFactory->get('desk_net.settings');
    $this->apiKey = $config->get('desk_net_api_key');
    $this->apiSecret = $config->get('desk_net_api_secret');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('request_stack'),
    $container->get('messenger'),
    $container->get('database'),
    $container->get('string_translation')
    );
  }

  /**
   * Generating custom form for "Drupal Credentials" page.
   */
  public function drupalCredentials() {
    $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();

    $html = '<h2>' . $this->stringTranslation->translate('Drupal Credentials') . '</h2>';
    $html .= '<p>';
    $html .= $this->stringTranslation->translate('Use these credentials in Desk-Net on the Advanced Settings tab of
	the <a href="https://www.desk-net.com/objectsPage.htm" target="_blank">platform</a> you are connecting to this Drupal website.');
    $html .= '</p>';

    $html .= '<table class="form-table">
	<tbody>
	<tr>
		<th scope="row">' . $this->stringTranslation->translate('API URL') . '</th>
		<td>' . $host . '/dr-json/v1</td>
	</tr>
	<tr>
		<th scope="row">' . $this->stringTranslation->translate('API User') . '</th>
		<td id="api_key">' . $this->apiKey . '</td>
	</tr>
	<tr>
		<th scope="row">' . $this->stringTranslation->translate('API Secret') . '</th>
		<td id="api_secret">' . $this->apiSecret . '</td>
	</tr>
	</tbody>
</table>';

    // Modal dialog.
    $html .= '<div id="consentModal" class="modal">
	<!-- Modal content -->
	<div class="modal-content">
		<span class="close">&times;</span>
		<p>' . $this->stringTranslation->translate('Are you sure to generate new credentials?') . '</p>
		<div class="position-control-element"><a id="confirm" href="' . $host . '/admin/config/desk-net/generate-new-credentials" class="button form-submit btn-primary-submit" value="Confirm">' . $this->stringTranslation->translate('Confirm') . '</a>
			<input type="button" name="cancel" id="cancel" class="button form-submit" value="' . $this->stringTranslation->translate('Cancel') . '"></div>
	</div>
</div>';

    $html .= '<input type="button" id="generate-new-credentials-submit" value="' . $this->stringTranslation->translate('Generate new credentials') . '" class="button form-submit">';

    $this->showMessage();

    return [
      '#allowed_tags' => [
        'input',
        'table',
        'tbody',
        'tr',
        'td',
        'th',
        'div',
        'span',
        'p',
        'h2',
        'a',
      ],
      '#markup' => $html,
    ];
  }

  /**
   * Showing message after successfully generate new credentials.
   */
  private function showMessage() {
    // Getting GET parameters from url and check on generate new credentials.
    $request = $this->requestStack->getCurrentRequest();
    $dn_credentials = $request->query->get('dn-credentials');

    if ($dn_credentials === 'generate') {
      $this->messenger->addStatus($this->stringTranslation->translate('New credentials successfully generated.'));
    }
  }

  /**
   * Perform generate Drupal token.
   *
   * @return string
   *   The generated Drupal token.
   */
  public static function generateDrupalModuleToken(): string {
    $token = Crypt::randomBytesBase64(32);
    // Hash the token using SHA-256.
    $token_hash = hash('sha256', $token);

    self::addTokenToList($token_hash);

    return $token;
  }

  /**
   * Performing remove expired tokens.
   */
  public static function removeExpiredTokens() {
    \Drupal::database()
      ->delete('desk_net_tokens')
      ->condition('expired_date', date('Y-m-d H:i:s'), '<')
      ->execute();
  }

  /**
   * Performing add a new token.
   *
   * @param string $token
   *   The token to add.
   */
  public static function addTokenToList($token) {
    $current_date_time = date('Y-m-d H:i:s');
    $expiration_date = date('Y-m-d H:i:s', strtotime($current_date_time . ' + 5 minutes'));
    $table_name = 'desk_net_tokens';

    \Drupal::database()->insert($table_name)
      ->fields([
        'token' => $token,
        'expired_date' => $expiration_date,
      ])
      ->execute();
  }

}
