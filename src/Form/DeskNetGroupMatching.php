<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ActionController;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Desk-Net group matching settings.
 */
class DeskNetGroupMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetGroupMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_group_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration_setting_name = 'desk_net_selected_group_matching_list';
    $desk_net_group_list = $this->getGroups();

    $platform_id = $this->configFactory->get('desk_net.settings')->get('platform_id');

    if (!empty($platform_id)) {
      if ($desk_net_group_list && !empty(ModuleSettings::variableGet('desk_net_token'))) {
        $html = '<h2>' . $this->stringTranslation->translate('Groups Matching') . '</h2>';
        $html .= '<p>';
        $html .= $this->stringTranslation->translate('Use this page to match the default group in Desk-Net on creating a Story from Drupal to Desk-Net.');
        $html .= '</p>';
        $html .= '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

        $form['html'] = [
          '#markup' => $html,
        ];

        $desk_net_to_drupal = [
          'default_group_in_desk_net' => [
            'id' => 'default_group_in_desk_net',
            'name' => 'Default Group in Desk-Net',
          ],
        ];

        $drupal_to_desk_net['no_sync'] = [
          'id' => 'no_sync',
          'name' => $this->stringTranslation->translate('No Sync'),
        ];

        foreach ($desk_net_group_list as $group) {
          $drupal_to_desk_net[$group['id']]['id'] = $group['id'];
          $drupal_to_desk_net[$group['id']]['name'] = $group['name'];
        }

        $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
        'group', '', 'no_sync');

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->stringTranslation->translate('Save'),
        ];

        return $form;
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          $formValues[$key] = $value;
        }
      }

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_group_matching_list', serialize($formValues))
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Retrieves groups from the Desk-Net platform.
   *
   * @return bool|array
   *   The result of loading the groups list from Desk-Net, or FALSE if
   *   unsuccessful.
   */
  private function getGroups() {
    $config = $this->configFactory->get('desk_net.settings');
    $platform_id = $config->get('platform_id');
    $saved_group_list_for_platform = $config->get('desk_net_group_list');

    if ($saved_group_list_for_platform !== NULL) {
      $saved_group_list_for_platform = unserialize($saved_group_list_for_platform, ['allowed_classes' => FALSE]);
    }

    if (!empty($platform_id)) {
      $group_list_for_platform = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'groups', '', 'v1_0_0');

      if ($group_list_for_platform === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(9));
        return $saved_group_list_for_platform;
      }

      $group_list_for_platform = json_decode($group_list_for_platform, TRUE);

      if (!empty($group_list_for_platform['message']) || $group_list_for_platform === 'not_show_new_notice' || empty($group_list_for_platform)) {
        return $saved_group_list_for_platform;
      }

      $this->resetDefaultGroupField($group_list_for_platform);

      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_group_list', serialize($group_list_for_platform))
        ->save();

      return $group_list_for_platform;
    }

    return NULL;
  }

  /**
   * Resets the default group field if the current default group is not in the provided group list for the platform.
   *
   * This function checks if the default selected group exists within the list of group IDs provided by the platform.
   * If the default selected group does not exist in the list, it updates the configuration to set the matching list to 'no_sync'.
   *
   * @param array $group_list_for_platform
   *   An array of groups available on the platform.
   *
   * @return void
   *   This function does not return a value.
   */
  private function resetDefaultGroupField($group_list_for_platform) {
    $configFactory = $this->configFactory->getEditable('desk_net.settings');
    $field_name = ActionController::generateFieldMatchingName('group', NULL, 'default_group_in_desk_net');
    $default_selected_group = ActionController::getFieldMatchingValue('desk_net_selected_group_matching_list', $field_name);

    $list_of_group_ids = array_column($group_list_for_platform, 'id');
    $exists = in_array($default_selected_group, $list_of_group_ids);

    if (!$exists) {
      $configFactory->set('desk_net_selected_group_matching_list', serialize(['desk_net_group_default_group_in_desk_net' => 'no_sync']))->save();
    }
  }

}
