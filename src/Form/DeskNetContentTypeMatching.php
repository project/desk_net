<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\DeleteMethods;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring content type matching settings.
 */
class DeskNetContentTypeMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetContentTypeMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation, DeleteMethods $delete_methods, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->deleteMethods = $delete_methods;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('entity_type.manager'),
    $container->get('string_translation'),
    $container->get('desk_net.delete_methods'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_content_type_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Loading all Drupal Content types from Desk-Net.
    $is_content_types_exist = $this->getTypes();
    $config = $this->configFactory->get('desk_net.settings');
    $configuration_setting_name = 'desk_net_selected_content_type_matching_list';
    $types_for_platform = $config->get('desk_net_types');

    if ($types_for_platform !== NULL) {
      $types_for_platform = unserialize($types_for_platform, ['allowed_classes' => FALSE]);
    }

    if (!empty($config->get('platform_id'))) {
      $html = '<h2>' . $this->stringTranslation->translate('Content Type Matching') . '</h2>';
      $html .= '<p>';
      $html .= $this->stringTranslation->translate('Use this page to match Types in Desk-Net to content types in Drupal.');
      $html .= '</p>';

      if ($is_content_types_exist && !empty(ModuleSettings::variableGet('desk_net_token'))) {
        $do_not_import = [
          'do_not_import' => [
            'id' => 'do_not_import',
            'name' => 'Do not import',
          ],
        ];
        $drupal_to_desk_net = [];
        // Load all Drupal Content types.
        $load_content_types = $this->entityTypeManager
          ->getStorage('node_type')
          ->loadMultiple();

        if (!empty($load_content_types)) {
          foreach ($load_content_types as $content_type) {
            $drupal_to_desk_net[$content_type->id()]['id'] = $content_type->id();
            $drupal_to_desk_net[$content_type->id()]['name'] = $content_type->label();

            // Creating custom Desk-Net fields for this content type if
            // they don't exist.
            ModuleSettings::createCustomFields($content_type->id());
          }
        }

        // Adding default menu item 'No category'.
        $desk_net_to_drupal['no_type']['id'] = 'no_type';
        $desk_net_to_drupal['no_type']['name'] = 'No Type';
        $drupal_to_desk_net = array_merge($do_not_import, $drupal_to_desk_net);

        if (!empty($types_for_platform)) {
          foreach ($types_for_platform as $value) {
            $desk_net_to_drupal[$value['id']]['id'] = $value['id'];
            $desk_net_to_drupal[$value['id']]['name'] = $value['name'];
          }
        }

        $html .= '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Desk-Net to Drupal') . '</h4>';

        $form['html'] = [
          '#markup' => $html,
        ];

        $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
        'type', 'desk_net_to_drupal', 'article'
        );

        $sub_title = '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

        $form['subTitle'] = [
          '#markup' => $sub_title,
        ];

        unset($drupal_to_desk_net['do_not_import']);
        $desk_net_to_drupal = array_merge($do_not_import, $desk_net_to_drupal);

        $form['drupal_to_desk_net_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
        $configuration_setting_name, $drupal_to_desk_net, $desk_net_to_drupal,
        'type', 'drupal_to_desk_net', 'no_type'
        );

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->stringTranslation->translate('Save'),
        ];

        return $form;
      }
      elseif (!$is_content_types_exist && empty($types_for_platform)) {
        $html .= '<p>';
        $html .= '<strong>' . $this->stringTranslation->translate('Please note:') . '</strong> ';
        $html .= $this->stringTranslation->translate('The synchronized platform in Desk-Net lacks platform types for matching.');
        $html .= '</p>';

        $form['html'] = [
          '#markup' => $html,
        ];

        return $form;
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          $formValues[$key] = $value;
        }
      }

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_content_type_matching_list', serialize($formValues))
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Perform get Types for Desk-Net platform.
   *
   * @return bool
   *   The result loading types list from Desk-Net.
   */
  private function getTypes() {
    $config = $this->configFactory->get('desk_net.settings');
    $configFactory = $this->configFactory->getEditable('desk_net.settings');
    $platform_id = $config->get('platform_id');
    $saved_types_list_for_platform = $config->get('desk_net_types');

    if ($saved_types_list_for_platform !== NULL) {
      $saved_types_list_for_platform = unserialize($saved_types_list_for_platform, ['allowed_classes' => FALSE]);
    }

    if (!empty($platform_id)) {
      $new_types_list = $this->requestsController->get(ModuleSettings::DN_BASE_URL,
      'types/platform', $platform_id);

      if ($new_types_list === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(3));

        return FALSE;
      }

      $new_types_list = json_decode($new_types_list, TRUE);

      if (empty($new_types_list)) {
        $configFactory->set('desk_net_types', serialize($new_types_list))
          ->save();
        return FALSE;
      }

      if (!empty($new_types_list['message']) || $new_types_list === 'not_show_new_notice') {
        return FALSE;
      }

      if (!empty($saved_types_list_for_platform)) {
        // Load all Drupal Content types.
        $load_content_types = $this->entityTypeManager
          ->getStorage('node_type')
          ->loadMultiple();

        $content_types_list = [];

        if (!empty($load_content_types)) {
          foreach ($load_content_types as $content_type) {
            $content_types_list[] = $content_type->id();
          }
        }

        $this->deleteMethods->shapeDeletedItems('desk_net_selected_content_type_matching_list', $new_types_list,
        $saved_types_list_for_platform, $content_types_list, 'type');
      }

      $configFactory->set('desk_net_types', serialize($new_types_list))->save();

      return TRUE;
    }

    return FALSE;
  }

}
