<?php

namespace Drupal\desk_net\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\DeleteMethods;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for test_api routes.
 */
class APIController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The API key for authentication.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The API secret for authentication.
   *
   * @var string
   */
  protected $apiSecret;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The ActionController instance.
   *
   * @var \Drupal\desk_net\Controller\ActionController
   */
  protected $actionController;

  /**
   * Constructs a new APIController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, DeleteMethods $delete_methods, LoggerChannelFactoryInterface $logger_factory, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->deleteMethods = $delete_methods;
    $this->loggerFactory = $logger_factory;
    $config = $this->configFactory->get('desk_net.settings');
    $this->apiKey = $config->get('desk_net_api_key');
    $this->apiSecret = $config->get('desk_net_api_secret');
    $this->stringTranslation = $string_translation;
  }

  /**
   * Sets the ActionController instance.
   *
   * This method is used to inject the ActionController dependency into this
   * RequestsController instance. This approach, known as setter injection,
   * helps to resolve circular dependencies by setting the dependency after
   * the object has been instantiated.
   *
   * @param \Drupal\desk_net\Controller\ActionController $action_controller
   *   The ActionController instance to be set.
   */
  public function setActionController(ActionController $action_controller) {
    $this->actionController = $action_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('entity_type.manager'),
    $container->get('desk_net.delete_methods'),
    $container->get('logger.factory'),
    $container->get('string_translation')
    );

    $instance->setActionController($container->get('desk_net.action_controller'));
    return $instance;
  }

  /**
   * Perform get Status list from Desk-Net.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string
   *   The status saving statuses.
   */
  public function saveStatuses(Request $request) {
    $response = new Response();
    // Token validation.
    if (!$this->isTokenValid($request)) {
      return $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }
    // This condition checks the `Content-type` and makes sure to
    // decode JSON string from the request body into array.
    if (str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), TRUE);
      $request->request->replace(is_array($data) ? $data : []);
    }
    else {
      return $response->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    $save_statuses_list = $this->configFactory->get('desk_net.settings')
      ->get('desk_net_activate_status_list');

    if (isset($data['platform'])) {
      $this->configFactory->getEditable('desk_net.settings')
        ->set('platform_id', $data['platform'])
        ->save();
    }
    else {
      return $response->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    $json_request = ModuleSettings::checkTriggersExportStatus($data);

    if (!empty($save_statuses_list)) {
      $save_statuses_list = unserialize($save_statuses_list, ['allowed_classes' => FALSE]);
      $drupal_status_list = ['published', 'unpublished'];

      $this->deleteMethods->shapeDeletedItems('desk_net_selected_status_matching_list',
      $json_request['activeStatuses'], $save_statuses_list, $drupal_status_list, 'status', FALSE);
    }
    if (!empty($json_request['activeStatuses'])) {
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_activate_status_list', serialize($json_request['activeStatuses']))
        ->save();

      $field_name = $this->actionController->generateFieldMatchingName(
      'status', 'desk_net_to_drupal', '5');
      $this->actionController->setFieldMatchingValue('desk_net_selected_status_matching_list', $field_name, '1');

      $field_name = $this->actionController->generateFieldMatchingName(
      'status', 'drupal_to_desk_net', '1');
      $this->actionController->setFieldMatchingValue('desk_net_selected_status_matching_list', $field_name, '5');
    }
    if (!empty($json_request['deactivatedStatuses'])) {
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_deactivate_status_list', serialize($json_request['deactivatedStatuses']))
        ->save();
    }

    return $response->setStatusCode(Response::HTTP_OK);
  }

  /**
   * Perform crete article in Drupal from Desk-Net.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string
   *   The result creating article in Drupal from Desk-Net.
   */
  public function createPublication(Request $request) {
    $response = new Response();
    // Token validation.
    if (!$this->isTokenValid($request)) {
      return $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }

    if (str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
      $data = json_decode($request->getContent(), TRUE);
      $request->request->replace(is_array($data) ? $data : []);
    }

    if (empty($data)) {
      return FALSE;
    }

    $response = $this->actionController->createNode($data);

    return new JsonResponse($response);
  }

  /**
   * Perform update article in Drupal from Desk-Net.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The node id which should updating.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string
   *   The result updating article in Drupal from Desk-Net.
   */
  public function updatePublication(RouteMatchInterface $route_match, Request $request) {
    $response = new Response();
    // Token validation.
    if (!$this->isTokenValid($request)) {
      return $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }

    $entity_id = $route_match->getRawParameter('story_id');
    $sync_updates_from_desk_net = $this->configFactory->get('desk_net.settings')
      ->get('sync_updates_from_desk_net');

    if ($sync_updates_from_desk_net || $sync_updates_from_desk_net === NULL) {
      if (str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), TRUE);
        $request->request->replace(is_array($data) ? $data : []);
      }

      if (empty($data) || empty($entity_id) || !is_numeric($entity_id)) {
        return $response->setStatusCode(Response::HTTP_ACCEPTED);
      }

      // Loading node by id.
      $node = $this->entityTypeManager->getStorage('node')->load($entity_id);
      if ($node !== NULL) {
        // Skipping updating slug if node was published.
        if ($node->status->value == 1 && isset($data['slug'])) {
          unset($data['slug']);
        }

        // Getting Desk-Net revision data.
        $desk_net_revision = ModuleSettings::deskNetRevisionGet($node);
        // Check node - removed status.
        if ($desk_net_revision !== FALSE && $desk_net_revision['desk_net_removed_status'] == 'desk_net_removed') {
          return $response->setStatusCode(Response::HTTP_ACCEPTED);
        }
      }
      else {
        return $response->setStatusCode(Response::HTTP_NOT_FOUND);
      }

      $response = $this->actionController->updateNode($data, $entity_id, $desk_net_revision);

      return new JsonResponse($response);
    }
    else {
      return new JsonResponse($this->actionController->generateDrupalResponseData($entity_id));
    }
  }

  /**
   * Perform delete node in Drupal by Desk-Net.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The node id for delete.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array|bool
   *   The result updating article status on 'Deleted/Removed' in Drupal.
   */
  public function deletePublication(RouteMatchInterface $route_match, Request $request) {
    $response = new Response();
    // Token validation.
    if (!$this->isTokenValid($request)) {
      return $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }

    $sync_deletions_from_desk_net = $this->configFactory->get('desk_net.settings')
      ->get('sync_deletions_from_desk_net');

    if ($sync_deletions_from_desk_net || $sync_deletions_from_desk_net === NULL) {
      // Getting cms_id.
      $node_id = $route_match->getRawParameter('story_id');

      // Getting value for matching "Deleted/Removed" on page "Status Matching".
      $field_name = $this->actionController->generateFieldMatchingName(
      'status', 'desk_net_to_drupal', 'desk_net_removed');
      $matching_status_remove = $this->actionController->getFieldMatchingValue(
      'desk_net_selected_status_matching_list', $field_name);

      if ($matching_status_remove === NULL) {
        $matching_status_remove = 0;
      }
      // Loading node by id.
      $node = $this->entityTypeManager->getStorage('node')->load($node_id);

      if ($node == NULL) {
        return $response->setStatusCode(Response::HTTP_NOT_FOUND);
      }
      // Getting Desk-Net revision data.
      $desk_net_revision = ModuleSettings::deskNetRevisionGet($node);
      // Adding information for the node about deleting story in Desk-Net app.
      $desk_net_revision['desk_net_removed_status'] = 'desk_net_removed';
      $node = ModuleSettings::deskNetRevisionSet($node, $desk_net_revision);
      // Updating node status.
      $node->set('status', $matching_status_remove);

      // Saving changed.
      $node->save();
    }

    return $response->setStatusCode(Response::HTTP_OK);
  }

  /**
   * Validating authorization Token.
   *
   * @param object $request
   *   The token.
   *
   * @return bool
   *   The result of checking.
   */
  protected function isTokenValid($request): bool {
    try {
      $token = $request->headers->get('authorization');

      if (empty($token)) {
        throw new \InvalidArgumentException("The client has not transmitted the token in the request.");
      }

      $tokenInfo = self::getTokenInfo($token);

      if (is_array($tokenInfo) && !$this->validateTokenInfo($tokenInfo)) {
        throw new \InvalidArgumentException("Invalid token or expired token.");
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('access denied')->warning($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Validate token information.
   *
   * @param array $tokenInfo
   *   The token information array.
   *
   * @return bool
   *   TRUE if the token information is valid, FALSE otherwise.
   */
  public static function validateTokenInfo(array $tokenInfo): bool {
    $current_date_time = date('Y-m-d H:i:s');
    $expiration_date = $tokenInfo['expire_date'] ?? 0;

    return $expiration_date >= $current_date_time;
  }

  /**
   * Perform generate new Desk-Net module Token.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string
   *   The status saving statuses.
   */
  public function generateAccessToken(Request $request) {
    $api_key = $request->headers->get('php-auth-user');
    $api_secret = $request->headers->get('php-auth-pw');

    if ($this->isValidDrupalCredentials($api_key, $api_secret)) {
      $token = DrupalCredentialsController::generateDrupalModuleToken();
      $response = new JsonResponse([
        'access_token' => $token,
        'token_type' => 'Bearer',
        'expires_in' => 300,
      ], 200, ['Content-Type' => 'application/json']);
    }
    else {
      $response = new JsonResponse([
        'message' => $this->stringTranslation->translate('Wrong credentials'),
      ], 401, ['Content-Type' => 'application/json']);
    }

    return $response;
  }

  /**
   * Checks if the provided API key and API secret match the stored credentials.
   *
   * @param string|null $api_key
   *   The API key provided.
   * @param string|null $api_secret
   *   The API secret provided.
   *
   * @return bool
   *   TRUE if the provided credentials match, FALSE otherwise.
   */
  protected function isValidDrupalCredentials($api_key, $api_secret): bool {
    if (NULL === $api_key || NULL === $api_secret) {
      return FALSE;
    }

    return $this->apiKey === $api_key && $this->apiSecret === $api_secret;
  }

  /**
   * Get token information from the custom table by token value.
   *
   * @param string $token
   *   The value of the token.
   *
   * @return array|null
   *   The token information array or NULL if not found.
   */
  protected static function getTokenInfo($token) {
    $token = hash('sha256', $token);
    $database = \Drupal::database();
    $table = 'desk_net_tokens';

    // Perform a database query to fetch token information.
    $query = $database->select($table, 't')
      ->fields('t')
      ->condition('t.token', $token)
      ->execute();

    $result = $query->fetchAssoc();

    return $result ?: NULL;
  }

}
