<?php

namespace Drupal\desk_net\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for managing settings related to the module.
 *
 * This controller provides functionality for managing settings related to
 * the module. It handles tasks such as configuration retrieval, modification,
 * and validation.
 */
class ModuleSettings extends ControllerBase {

  // Desk-Net Rest Service.
  const DN_BASE_URL = 'https://kordiam.app';

  /**
   * The translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Constructs a ModuleSettings object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('string_translation')
    );
  }

  /**
   * Retrieves the statuses for Desk-Net.
   *
   * This method retrieves the statuses for Desk-Net and returns them as
   * markup for rendering in the Drupal application. The statuses may include
   * information about the generation of new credentials.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return array
   *   An array containing the markup for rendering the Desk-Net statuses.
   */
  public function getStatuses(RouteMatchInterface $route_match, Request $request) {
    return [
      '#markup' => $this->stringTranslation->translate('Desk-Net Generate New Credentials'),
    ];
  }

  /**
   * Checks whether Desk-Net user credentials are configured in Drupal.
   *
   * @return bool
   *   TRUE if Desk-Net user credentials are configured, FALSE otherwise.
   */
  public static function checkUserCredentials() {
    $config = \Drupal::config('desk_net.settings');
    $api_user = $config->get('desk_net_api_user');
    $api_password = $config->get('desk_net_api_password');

    return !((empty($api_user) || empty($api_password)));
  }

  /**
   * Getting custom Desk-Net variables.
   *
   * @param string $key
   *   The variable key.
   *
   * @return bool|array|string|int
   *   The result of getting Desk-Net variables.
   */
  public static function variableGet($key) {
    $q = \Drupal::database()->select('desk_net_variable', 'v');
    $q->fields('v', ['value']);
    $q->condition('v.name', $key);
    $value = $q->execute()->fetchField();
    if ($value) {
      $value = unserialize($value, ['allowed_classes' => FALSE]);
      if ($key === 'desk_net_token' && ($encryption_key = \Drupal::config('desk_net.settings')
        ->get('desk_net_encryption_key'))) {
        $value = self::decrypt($value, $encryption_key);
      }
      return $value;
    }

    return NULL;
  }

  /**
   * Setting custom Desk-Net variables.
   *
   * @param string $key
   *   The variable key.
   * @param string|array|int|bool $value
   *   The variable value.
   *
   * @return bool
   *   The result of setting Desk-Net variables.
   */
  public static function variableSet($key, $value): bool {
    if ($key === 'desk_net_token' && ($encryption_key = \Drupal::config('desk_net.settings')
      ->get('desk_net_encryption_key'))) {
      $value = self::encrypt($value, $encryption_key);
    }
    $variable = \Drupal::database()->merge('desk_net_variable')
      ->keys(['`name`' => $key])
      ->insertFields([
        '`name`' => $key,
        '`value`' => serialize($value),
      ])
      ->updateFields([
        '`value`' => serialize($value),
      ])
      ->execute();

    if ($variable) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Performs delete custom Desk-Net variables.
   *
   * @param string $key
   *   The variable key.
   *
   * @return bool
   *   The result of deleting Desk-Net variables.
   */
  public static function variableDel($key) {
    $value = \Drupal::database()->delete('desk_net_variable')
      ->condition('name', $key)
      ->execute();

    if ($value) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Getting value Desk-Net revision fields.
   *
   * @param object $node
   *   The entity object.
   *
   * @return bool|string
   *   The result of loading Desk-Net revision.
   */
  public static function deskNetRevisionGet($node) {
    // Getting hash field name.
    $hash_field_name = ModuleSettings::variableGet('desk_net_' . $node->bundle() . '_revision');

    // If field name with Desk-Net revision data was not found.
    if ($hash_field_name === NULL) {
      return FALSE;
    }
    // If field with Desk-Net data was not found.
    if (!($node->__isset($hash_field_name))) {
      return FALSE;
    }

    // Getting Desk-Net revision data.
    $desk_net_revision = $node->get($hash_field_name)->value;

    if ($desk_net_revision == NULL) {
      return NULL;
    }

    try {
      // Json decode.
      $result = json_decode($desk_net_revision, TRUE);
    }
    catch (\Exception $e) {
      \Drupal::logger('Get field Desk-Net Revision')->warning($e->getMessage());

      return FALSE;
    }

    return $result;
  }

  /**
   * Setting value Desk-Net revision fields.
   *
   * @param object $node
   *   The entity object.
   * @param array $data
   *   The new data for updating.
   *
   * @return bool|object
   *   The result of loading Desk-Net revision.
   */
  public static function deskNetRevisionSet($node, array $data) {
    // Getting hash field name.
    $hash_field_name = ModuleSettings::variableGet('desk_net_' . $node->bundle() . '_revision');
    // If field name with Desk-Net revision data was not found.
    if ($hash_field_name === NULL) {
      return FALSE;
    }

    try {
      // Json encode.
      $result = json_encode($data);
    }
    catch (\Exception $e) {
      \Drupal::logger('Set field Desk-Net Revision')->warning($e->getMessage());

      return FALSE;
    }

    // Updating Desk-Net revision data.
    return $node->set($hash_field_name, (string) $result);
  }

  /**
   * Perform create text field to node.
   *
   * @param array $story_data
   *   The old data from DN.
   * @param object $entity
   *   The new node data.
   * @param string $content_type
   *   The element content type.
   *
   * @return array|bool|string
   *   The result of updating JSON with Desk-Net data, before send to Desk-Net.
   */
  public static function updateDataBeforeSendToDn(array $story_data, $entity, $content_type) {
    // Getting Desk-Net revision data.
    $desk_net_revision = ModuleSettings::deskNetRevisionGet($entity);
    // Default category value: 'No Category'.
    $selected_category = NULL;
    // Default category if not found revision.
    $revision_category = NULL;
    // Set publication position.
    $publication_position = self::getPublicationPosition(
      $story_data['publications'], $desk_net_revision['desk_net_publications_id']);

    // Updating Slug value.
    $initial_slug_syncing = \Drupal::config('desk_net.settings')
      ->get('initial_slug_syncing');

    if (!$initial_slug_syncing) {
      $story_data = ActionController::sendSlugByCms($entity, $story_data);
    }
    // Get entity id.
    $entity_id = $entity->id();
    // Loading node by id.
    $entity = Node::load($entity_id);
    // Update elements.
    $cms_links = ActionController::getCmsLinks($entity_id);

    $story_data['publications'][$publication_position]['url_to_published_content'] = $cms_links['cmsOpenLink'];
    if (!empty($cms_links['cmsOpenLinkTitle'])) {
      $story_data['publications'][$publication_position]['url_to_published_content_title'] = $cms_links['cmsOpenLinkTitle'];
    }

    $story_data['publications'][$publication_position]['url_to_content_in_cms'] = $cms_links['cmsEditLink'];
    if (!empty($cms_links['cmsEditLinkTitle'])) {
      $story_data['publications'][$publication_position]['url_to_content_in_cms_title'] = $cms_links['cmsEditLinkTitle'];
    }

    // Loading penultimate revision data, before change entity.
    $node_storage = \Drupal::service('entity_type.manager')->getStorage('node');
    $penultimate_node_revision = $node_storage->loadRevision((int) $entity->vid->value - 1);

    // Checking status changes.
    $initial_status_syncing = (bool) \Drupal::config('desk_net.settings')
      ->get('initial_status_syncing') ?? FALSE;

    if (isset($entity->status->value) && !$initial_status_syncing) {
      // Loading status by Status Matching.
      $field_name = ActionController::generateFieldMatchingName(
        'status', 'drupal_to_desk_net', $entity->status->value);
      $status_id = ActionController::getFieldMatchingValue('desk_net_selected_status_matching_list', $field_name);
      // Checking Status Matching on default value after install module.
      if ($status_id !== 'no_sync') {
        if ($status_id !== NULL && $status_id != 0) {
          $story_data['publications'][$publication_position]['status'] = (int) $status_id;
        }
        else {
          $story_data['publications'][$publication_position]['status'] = 1;
        }
      }
    }

    // Updating author.
    $user_data = User::load($entity->getOwnerId());
    $user = 'anonymous';

    // Checking user on anonymity.
    if ($user_data === NULL || $user_data->uid->value == '0') {
      $account = \Drupal::currentUser();

      if ($account->id() == 1) {
        $user = $account;
      }
    }
    else {
      $user = $user_data;
    }

    // Getting hash field name.
    $hash_author_field_name = ModuleSettings::variableGet('desk_net_author_id');

    if ($user !== 'anonymous' && $user_data->__isset($hash_author_field_name) && $user_data->get($hash_author_field_name)->value !== NULL) {
      $story_data['tasks'][0]['user'] = intval($user_data->get($hash_author_field_name)->value);
    }

    // Setting active Category ID.
    if (isset($penultimate_node_revision->field_tag) && isset($entity->field_tags) && !empty($entity->field_tags->referencedEntities())) {
      $selected_category = $entity->field_tags->referencedEntities();
      // Loading penultimate revision category, before change entity.
      $revision_category = $penultimate_node_revision->field_tags->referencedEntities();
    }
    // Checking update category.
    if ($penultimate_node_revision == NULL || $selected_category != $revision_category) {
      // Updating category value.
      if (!empty($selected_category)) {
        // Select active Category ID.
        if ($entity->__isset('field_tags')) {
          $story_data = ActionController::settingCategoryDrupalToDeskNet($entity, $story_data, $publication_position);
        }
      }
    }
    // Setting type.
    $field_name = ActionController::generateFieldMatchingName(
      'type', 'drupal_to_desk_net', $entity->bundle());
    $content_type_matching = ActionController::getFieldMatchingValue(
      'desk_net_selected_content_type_matching_list', $field_name);

    if ($content_type_matching !== NULL && $content_type_matching !== 'no_type' && $content_type_matching !== 'do_not_import') {
      $story_data['publications'][$publication_position]['type'] = (int) $content_type_matching;
    }
    elseif ('do_not_import' === $content_type_matching) {
      return 'do_not_import';
    }

    $drupal_publication_cf = ['title', 'url_alias'];

    foreach ($drupal_publication_cf as $publication_cf) {
      $field_name = ActionController::generateFieldMatchingName(
        'publication_cf', 'drupal_to_desk_net', $publication_cf);
      $matching_publication_cf = ActionController::getFieldMatchingValue(
        'desk_net_selected_publication_custom_field_matching_list', $field_name);

      if (empty($matching_publication_cf) || $matching_publication_cf === 'no_publication_cf') {
        continue;
      }

      $story_data = ActionController::setPublicationCustomFieldToStory($entity, $publication_cf, $story_data, $matching_publication_cf, $publication_position);
    }

    return $story_data;
  }

  /**
   * Retrieves the position of a publication within a list.
   *
   * This method takes a list of publications and a publication ID, and
   * returns the position of the specified publication within the list. If the
   * publication ID is not found in the list, it defaults to position 0.
   *
   * @param array $publications_list
   *   The list of publications.
   * @param string|null $publication_id
   *   The ID of the publication whose position is to be found.
   *
   * @return int
   *   The position of the publication in the list. Defaults to 0 if not found.
   */
  public static function getPublicationPosition($publications_list, $publication_id) {
    // Set publication position.
    $publication_position = 0;
    // Get publication position from publication list.
    if ($publication_id !== NULL) {
      $publications_id = $publication_id;

      foreach ((array) $publications_list as $key => $value) {
        if ($publications_list[$key]['id'] == $publications_id) {
          $publication_position = $key;
          break;
        }
      }
    }

    return $publication_position;
  }

  /**
   * Perform update activate and deactivate status list.
   *
   * Update active statuses by triggersExport.
   *
   * @param array $status_list
   *   The default status list.
   *
   * @return array
   *   The refreshed status list.
   */
  public static function checkTriggersExportStatus(array $status_list) {
    if (empty($status_list['deactivatedStatuses'])) {
      $status_list['deactivatedStatuses'] = [];
    }
    foreach ($status_list['activeStatuses'] as $key => $value) {
      if ($status_list['activeStatuses'][$key]['triggersExport'] == FALSE) {
        array_unshift($status_list['deactivatedStatuses'],
          $status_list['activeStatuses'][$key]);
        unset($status_list['activeStatuses'][$key]);
      }
    }
    return $status_list;
  }

  /**
   * Adding new text fields to articles.
   *
   * @param string $content_type
   *   The content-type name.
   */
  public static function createCustomFields($content_type) {
    ModuleSettings::createCustomField('desk_net_' . $content_type . '_revision', $content_type, 'node');
  }

  /**
   * Adding new text field to node.
   *
   * @param string $field_name
   *   The field name.
   * @param string $bundle
   *   The field type.
   * @param string $entity_type
   *   The entity type.
   */
  public static function createCustomField($field_name, $bundle = 'article', $entity_type = 'node') {
    if (ModuleSettings::variableGet($field_name) === NULL) {
      // Generating unique name.
      $hash_name = 'dn_' . substr(md5($field_name), 0, 16);
      // Creating/updating field matching.
      ModuleSettings::variableSet($field_name, $hash_name);

      if (FieldStorageConfig::loadByName($entity_type, $hash_name) == NULL) {
        $field_storage = FieldStorageConfig::create([
          'field_name' => $hash_name,
          'entity_type' => $entity_type,
          'type' => 'text',
          'settings' => ['max_length' => 1024],
        ]);
        $field_storage->save();

        $field = FieldConfig::create([
          'field_name' => $hash_name,
          'entity_type' => $entity_type,
          'bundle' => $bundle,
          'label' => $hash_name,
        ]);
        $field->save();
      }
    }
  }

  /**
   * Encrypts a given value using the provided encryption key.
   *
   * This method uses the Sodium library to perform encryption. A nonce is
   * generated for each encryption operation to ensure that the same plaintext
   * will not produce the same ciphertext. The encrypted value is then returned
   * as a base64 encoded string combining the nonce and the ciphertext.
   *
   * @param string $value
   *   The plaintext value to be encrypted.
   * @param string $key
   *   The base64 encoded encryption key. Marked as sensitive to avoid
   *   accidental logging.
   *
   * @return string
   *   The base64 encoded string containing the nonce and the ciphertext.
   */
  private static function encrypt(string $value, #[\SensitiveParameter] string $key): string {
    $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
    $ciphertext = sodium_crypto_secretbox($value, $nonce, base64_decode($key));
    return base64_encode($nonce . $ciphertext);
  }

  /**
   * Decrypts a given encrypted value using the provided encryption key.
   *
   * This method uses the Sodium library to perform decryption. The nonce is
   * extracted from the beginning of the encrypted value, and the remaining part
   * is treated as the ciphertext. The method returns the decrypted plaintext or
   * FALSE if decryption fails.
   *
   * @param string $value
   *   The base64 encoded string containing the nonce and the ciphertext.
   * @param string $key
   *   The base64 encoded encryption key. Marked as sensitive to avoid
   *   accidental logging.
   *
   * @return false|string
   *   The decrypted plaintext string if successful,
   *   or FALSE if decryption fails.
   */
  private static function decrypt(string $value, #[\SensitiveParameter] string $key): false|string {
    $value = base64_decode($value);
    if ($value !== FALSE) {
      $nonce = mb_substr($value, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
      $ciphertext = mb_substr($value, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, NULL, '8bit');
      $value = sodium_crypto_secretbox_open($ciphertext, $nonce, base64_decode($key));
    }
    return $value;
  }

}
