<?php

namespace Drupal\desk_net\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controller methods for handling Desk-Net API requests.
 */
class RequestsController extends ControllerBase {

  /**
   * The HTTP client to make requests.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ActionController instance.
   *
   * @var \Drupal\desk_net\Controller\ActionController
   */
  protected $actionController;

  /**
   * Constructs a RequestsController object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client to make requests.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('desk_net');
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the ActionController instance.
   *
   * This method is used to inject the ActionController dependency into this
   * RequestsController instance. This approach, known as setter injection,
   * helps to resolve circular dependencies by setting the dependency after
   * the object has been instantiated.
   *
   * @param \Drupal\desk_net\Controller\ActionController $action_controller
   *   The ActionController instance to be set.
   */
  public function setActionController(ActionController $action_controller) {
    $this->actionController = $action_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
    $container->get('http_client'),
    $container->get('logger.factory'),
    $container->get('config.factory'),
    $container->get('entity_type.manager')
    );

    $instance->setActionController($container->get('desk_net.action_controller'));
    return $instance;
  }

  /**
   * Getting the Desk-Net TOKEN.
   *
   * @param string $api_user
   *   The Desk-Net user login.
   * @param string $api_password
   *   The Desk-Net user password.
   *
   * @return string
   *   The result getting token from Desk-Net.
   */
  public function getToken($api_user, $api_password) {
    // Building URL.
    $url = ModuleSettings::DN_BASE_URL . '/api/token';
    // Request options.
    $options = [
      'verify' => TRUE,
      'form_params' => [
        'grant_type' => 'client_credentials',
        'client_id' => $api_user,
        'client_secret' => $api_password,
      ],
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
      'http_errors' => FALSE,
    ];

    try {
      $response = $this->httpClient->request('POST', $url, $options);

      $code = $response->getStatusCode();
    }
    catch (RequestException $e) {
      $this->logger->notice($e->getMessage());

      return FALSE;
    }

    if ($code != 200) {
      ModuleSettings::variableSet('desk_net_token', 'not_valid');

      return FALSE;
    }
    // Update token option.
    $response_data = json_decode($response->getBody()->getContents(), TRUE);
    ModuleSettings::variableSet('desk_net_token', $response_data['access_token']);

    return $response_data['access_token'];
  }

  /**
   * Perform HTTP POST/DELETE request.
   *
   * @param string $http_request
   *   Custom HTTP request.
   * @param array $data
   *   The upload data.
   * @param string $url
   *   Base API url.
   * @param string $type
   *   The Desk-Net API method.
   * @param string $record_id
   *   The ID element.
   *
   * @return string
   *   The additional information about story from Desk-Net.
   */
  public function customRequest($http_request, array $data, $url, $type, $record_id = '') {
    $request_url = $url . "/api/v1_0_1/{$type}/{$record_id}";

    $data = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    // Delete ampersand encode.
    $data = str_replace('&amp;', '&', $data);

    $token = $this->refreshDeskNetToken();

    if (!$token) {
      return 'unauthorized';
    }

    $options = [
      'headers' => [
        'Content-Type' => 'application/json;charset=UTF-8',
        'Content-Length' => strlen($data),
        'Authorization' => 'bearer ' . $token,
      ],
      'body' => $data,
      'http_errors' => FALSE,
    ];

    return $this->sendRequest($http_request, $request_url, $options, $data);
  }

  /**
   * Perform HTTP GET request.
   *
   * @param string $url
   *   Base API url.
   * @param string $type
   *   The Desk-Net API method.
   * @param string $record_id
   *   ID element.
   * @param string $api_version
   *   Desk-Net API version.
   *
   * @return string
   *   The request result.
   */
  public function get($url, $type, $record_id = '', $api_version = 'v1_0_1') {
    $token = $this->refreshDeskNetToken();

    if (!$token) {
      return 'unauthorized';
    }

    $request_url = $url . "/api/{$api_version}/{$type}/{$record_id}";

    $options = [
      'headers' => [
        'Authorization' => 'bearer ' . $token,
      ],
      'http_errors' => FALSE,
    ];

    return $this->sendRequest('GET', $request_url, $options);
  }

  /**
   * Perform send HTTP Request.
   *
   * @param string $http_request
   *   Custom HTTP request.
   * @param string $request_url
   *   The link for request.
   * @param array $options
   *   The params for request.
   * @param string $data
   *   The post data.
   * @param bool $last_request
   *   The attempt send request.
   *
   * @return string
   *   The request result.
   */
  private function sendRequest($http_request, $request_url, array $options, $data = NULL, $last_request = NULL) {
    try {
      $obj_response = $this->httpClient->request($http_request, $request_url, $options);
    }
    catch (RequestException $e) {
      $this->logger->notice($e->getMessage());

      return FALSE;
    }

    $http_code = $obj_response->getStatusCode();

    switch ($http_code) {
      case 401:
        $response = $this->authorization($http_request, $request_url, $options, $data, $last_request);
        break;

      case 400:
        if (empty($obj_response->getBody()->__toString())) {
          return FALSE;
        }
        $response = $this->checkPlatformSchedule($obj_response->getBody()->__toString());
        break;

      case 200:
        $response = $obj_response->getBody()->__toString();
        break;

      default:
        if (empty($obj_response->getBody()->__toString())) {
          return FALSE;
        }

        if ($data !== NULL) {
          $data = json_decode($data, TRUE);
        }
        else {
          $data = [];
        }

        $body_response = json_decode($obj_response->getBody()->__toString(), TRUE);

        if (!empty($body_response)) {
          $response = json_encode($body_response);

          if (!empty($body_response['message'])) {
            $this->logger->error('Desk-Net API Error - HTTP Request Method {http_request}: {message}', [
              'http_request' => $http_request,
              'message' => $body_response['message'],
            ]);
          }

          if (isset($data['publications'][0]['cms_id'])) {
            $response = $this->checkErrorMessage($body_response, $data['publications'][0]['cms_id']);
          }
        }
        else {
          $response = 'not_show_new_notice';
        }
    }

    return $response;
  }

  /**
   * Perform check user authorized.
   *
   * @param string $http_request
   *   Custom HTTP request.
   * @param string $request_url
   *   The link for request.
   * @param array $args
   *   The params for request.
   * @param string $data
   *   The post data.
   * @param bool $last_request
   *   The attempt send request.
   *
   * @return string|array
   *   The result getting new token.
   */
  private function authorization($http_request, $request_url, array $args, $data, $last_request) {
    $token = $this->refreshDeskNetToken(TRUE);

    if ($token !== FALSE) {
      ModuleSettings::variableSet('desk_net_token', $token);
    }

    $response = 'unauthorized';

    if ($last_request != 'update_token' && $token !== FALSE) {
      $args['headers']['Authorization'] = "bearer $token";
      $response = $this->sendRequest($http_request, $request_url, $args, $data, 'update_token');
    }

    return $response;
  }

  /**
   * Retrieves Desk-Net API credentials from Drupal configuration.
   *
   * @return array
   *   An array containing Desk-Net API user and password.
   */
  private function getDeskNetCredentials() {
    $config = $this->configFactory->get('desk_net.settings');
    $api_user = $config->get('desk_net_api_user');
    $api_password = $config->get('desk_net_api_password');

    return ['api_user' => $api_user, 'api_password' => $api_password];
  }

  /**
   * Refreshes the DeskNet token.
   *
   * This function refreshes the DeskNet token. If the token is empty or set to
   * 'not_valid', it retrieves the new Desk-Net token.
   *
   * @param bool $forceRefresh
   *   A flag indicating whether to force a token refresh.
   *
   * @return string
   *   The Desk-Net token.
   */
  private function refreshDeskNetToken(bool $forceRefresh = FALSE) {
    $token = ModuleSettings::variableGet('desk_net_token');

    if ($forceRefresh || empty($token) || $token === 'not_valid') {
      $desk_net_credentials = $this->getDeskNetCredentials();
      $token = $this->getToken($desk_net_credentials['api_user'], $desk_net_credentials['api_password']);
    }

    return !$token ? NULL : $token;
  }

  /**
   * Perform check platform schedule in response from request.
   *
   * @param object $response
   *   The request response.
   *
   * @return string|array
   *   The request mark.
   */
  private function checkPlatformSchedule($response) {
    $body_response = json_decode($response, TRUE);

    if (str_starts_with($body_response['message'], "Publication date is prior to deadline date")) {
      $response = 'platform_schedule';
    }
    return $response;
  }

  /**
   * Perform check request Error Message from Desk-Net.
   *
   * @param array $message_content
   *   The list message.
   * @param string $node_id
   *   The node ID in Drupal.
   *
   * @return array|string
   *   The result of scanning - error message.
   */
  private function checkErrorMessage(array $message_content, $node_id) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $user_storage = $this->entityTypeManager->getStorage('user');

    if (preg_match('/^Page with id \[\d.*\] was not found/', $message_content['message'])) {
      $node = $node_storage->load($node_id);
      $this->actionController->createNodeDrupalToDn($node);

      return 'not_show_new_notice';
    }
    elseif (preg_match('/^User with id \[\d.*\] was not found/', $message_content['message'])) {
      preg_match('/\d.*\d/', $message_content['message'], $desk_net_user_id);
      $node = $node_storage->load($node_id);
      $user = $user_storage->load($node->getOwnerId());
      // Getting hash field name.
      $hash_field_name = ModuleSettings::variableGet('desk_net_author_id');

      if (isset($user->get($hash_field_name)->value) && !empty($user->get($hash_field_name)->value)) {
        $user->set($hash_field_name, NULL);
        $user->save();
      }
      else {
        return 'not_show_new_notice';
      }

      $this->actionController->createNodeDrupalToDn($node);

      return 'not_show_new_notice';
    }
    else {
      return json_encode($message_content);
    }
  }

}
