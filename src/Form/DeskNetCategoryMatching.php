<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\ModuleSettings;
use Drupal\desk_net\Controller\RequestsController;
use Drupal\desk_net\DeleteMethods;
use Drupal\desk_net\PageTemplate\PageTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Desk-Net category matching settings.
 */
class DeskNetCategoryMatching extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The delete methods service.
   *
   * @var \Drupal\desk_net\DeleteMethods
   */
  protected $deleteMethods;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetCategoryMatching form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\DeleteMethods $delete_methods
   *   The delete methods service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation, DeleteMethods $delete_methods, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->deleteMethods = $delete_methods;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('entity_type.manager'),
    $container->get('string_translation'),
    $container->get('desk_net.delete_methods'),
    $container->get('desk_net.requests_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_category_matching';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $drupal_to_desk_net = [];
    $config = $this->configFactory->get('desk_net.settings');
    $configuration_setting_name = 'desk_net_selected_category_matching_list';
    $load_category_status = $this->getCategory();
    $desk_net_category_list = $config->get('desk_net_category_list');

    if ($desk_net_category_list !== NULL) {
      $desk_net_category_list = unserialize($desk_net_category_list, ['allowed_classes' => FALSE]);
    }

    if (!empty($config->get('platform_id'))) {
      if (!empty(ModuleSettings::variableGet('desk_net_token'))) {
        if ((!$load_category_status && !empty($desk_net_category_list)) || $load_category_status !== FALSE) {
          $drupal_category_list = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();

          if (!empty($drupal_category_list['tags'])) {
            $vocabulary_term_list = $this->entityTypeManager
              ->getStorage('taxonomy_term')
              ->loadTree($drupal_category_list['tags']->id());

            if (!empty($vocabulary_term_list)) {
              foreach ($vocabulary_term_list as $term) {
                $drupal_to_desk_net[$term->tid]['id'] = $term->tid;
                $drupal_to_desk_net[$term->tid]['name'] = $term->name;

                $parents_list = $term->parents;

                if ($parents_list[0] !== '0') {
                  // Not realize case for two or more parents.
                  $drupal_to_desk_net[$term->tid]['parent'] = $parents_list[0];
                }
              }
            }
          }

          // Adding default menu item 'No category'.
          $desk_net_to_drupal['no_category']['id'] = 'no_category';
          $desk_net_to_drupal['no_category']['name'] = 'No category';

          foreach ($desk_net_category_list as $value) {
            $desk_net_to_drupal[$value['id']]['id'] = $value['id'];
            $desk_net_to_drupal[$value['id']]['name'] = $value['name'];
            if (isset($value['category'])) {
              $desk_net_to_drupal[$value['id']]['parent'] = $value['category'];
            }
          }

          $html = '<h2>' . $this->stringTranslation->translate('Category Matching') . '</h2>';
          $html .= $this->stringTranslation->translate('Use this page to match categories in Desk-Net to those in Drupal and vice versa.');
          $html .= '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Desk-Net to Drupal') . '</h4>';

          $form['html'] = [
            '#markup' => $html,
          ];

          $drupal_to_desk_net_new_elements['no_category']['id'] = 'no_category';
          $drupal_to_desk_net_new_elements['no_category']['name'] = 'No category';
          $drupal_to_desk_net_new_elements['do_not_import']['id'] = 'do_not_import';
          $drupal_to_desk_net_new_elements['do_not_import']['name'] = 'Do not import';

          $drupal_to_desk_net = array_merge($drupal_to_desk_net_new_elements, $drupal_to_desk_net);

          $form['desk_net_to_drupal_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
          $configuration_setting_name, $desk_net_to_drupal, $drupal_to_desk_net,
          'category', 'desk_net_to_drupal', 'no_category'
          );

          unset($drupal_to_desk_net['do_not_import'], $drupal_to_desk_net['no_category']);

          $desk_net_to_drupal = array_merge($drupal_to_desk_net_new_elements, $desk_net_to_drupal);

          $sub_title = '<h4 class="dn_b-title">' . $this->stringTranslation->translate('Drupal to Desk-Net') . '</h4>';

          if (empty($drupal_to_desk_net)) {
            $sub_title .= '<p>' . $this->stringTranslation->translate('This platform does not contain any tags to which categories could be matched.') . '</p>';
            $form['subTitle'] = [
              '#markup' => $sub_title,
            ];
          }
          else {
            $form['subTitle'] = [
              '#markup' => $sub_title,
            ];
            $form['drupal_to_desk_net_matching'] = PageTemplate::generateDeskNetMatchingPageTemplate(
            $configuration_setting_name, $drupal_to_desk_net, $desk_net_to_drupal,
            'category', 'drupal_to_desk_net', 'no_category'
            );
          }

          $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->stringTranslation->translate('Save'),
          ];

          return $form;
        }
        else {
          $this->messenger->addError(NoticesCollection::getNotice(9));
        }
      }
      else {
        $this->messenger->addError(NoticesCollection::getNotice(9));
      }
    }
    else {
      $this->messenger->addError(NoticesCollection::getNotice(10));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = NULL;

    if (!empty($form_state->getValues())) {
      foreach ($form_state->getValues() as $key => $value) {
        if ($key != 'form_id' && $key != 'op' && $key != 'form_token' &&
                    $key != 'form_build_id' && $key != 'submit'
        ) {
          // Save value.
          $formValues[$key] = $value;
        }
      }

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_selected_category_matching_list', serialize($formValues))
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

  /**
   * Perform get Category list from Desk-Net.
   *
   * @return bool|array
   *   The result loading category list from Desk-Net.
   */
  private function getCategory() {
    $config = $this->configFactory->get('desk_net.settings');
    $platform_id = $config->get('platform_id');

    $save_category_list_for_platform = $config->get('desk_net_category_list');

    if ($save_category_list_for_platform !== NULL) {
      $save_category_list_for_platform = unserialize($save_category_list_for_platform, ['allowed_classes' => FALSE]);
    }

    if (!empty($platform_id)) {
      $category_list = $this->requestsController->get(ModuleSettings::DN_BASE_URL, 'categories/platform', $platform_id);

      if ($category_list === 'unauthorized') {
        $this->messenger->addError(NoticesCollection::getNotice(9));

        return FALSE;
      }

      $category_list = json_decode($category_list, TRUE);

      if (!empty($category_list['message']) || $category_list === 'not_show_new_notice'
                || empty($category_list)) {
        return FALSE;
      }

      if ($save_category_list_for_platform) {
        $element_list_id = [];
        // Loading Drupal vocabularies.
        $drupal_category_list = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();

        if (isset($drupal_category_list['tags']) && $drupal_category_list['tags'] != NULL) {
          $vocabulary_term_list = $this->entityTypeManager
            ->getStorage('taxonomy_term')
            ->loadTree($drupal_category_list['tags']->id());
        }
        else {
          return FALSE;
        }

        if (!empty($vocabulary_term_list)) {
          foreach ($vocabulary_term_list as $term) {
            $element_list_id[] = $term->tid;
          }
        }

        $this->deleteMethods->shapeDeletedItems('desk_net_selected_category_matching_list', $category_list, $save_category_list_for_platform, $element_list_id, 'category');
      }

      $this->configFactory->getEditable('desk_net.settings')
        ->set('desk_net_category_list', serialize($category_list))
        ->save();

      return $category_list;
    }

    return FALSE;
  }

}
