CONTENTS OF THIS FILE
--------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Are you using the editorial calendar software Desk-Net to plan your stories and
to manage your editorial department?

This module connects your Desk-Net account to one or multiple Drupal sites.
Your story lists and related meta-data such as statuses are synced both ways
between the two systems.

* For a full description of the module visit
  (https://support.desk-net.com/hc/en-us/articles/
  115002751751-How-It-Works-Desk-Net-Module-for-Drupal)


* To submit bug reports and feature suggestions, or to track changes visit
  https://www.drupal.org/project/issues/advanced_help

REQUIREMENTS
------------

This module has no required dependencies outside of Drupal core.


RECOMMENDED MODULES
-------------------

* Advanced Help Hint (https://www.drupal.org/project/advanced_help_hint):
  If Advanced Help is not enabled, this module will generate a hint string that
  can be used in the project's hook_help to hint that Advanced Help should be
  enabled.

INSTALLATION
------------

Install the Advanced Help module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/895232 for more information.


CONFIGURATION
--------------

By itself, this module doesn't do much. It assists other modules and themes in
showing help texts. Nothing will show up until you enable at least one other
module that makes use of the Advanced Help framework or comes with a file named
README.md or README.txt.


MAINTAINERS
-----------

* David Valdez - https://www.drupal.org/u/gnuget

== Description ==

Are you using the editorial calendar software Desk-Net to plan your stories and
to manage your editorial department?

This module connects your Desk-Net account to one or multiple Drupal sites.
Your story lists and related meta-data such as statuses are synced both ways
between the two systems.

Two main use-cases are supported:

* Planned content: A article is planned and enriched with meta-data in Desk-Net.
  Based on this Desk-Net automatically creates a article in Drupal. Subsequent
  changes in either system are synced instantly.
* Breaking news: A story originates as a article in Drupal. The plugin sends
  relevant data to Desk-Net ensuring the story lists are complete.

The following meta-data is synced:

* Article description
* Publishing date and time
* Statuses
* Categories
* Author emails
* URLs of either system

Please note that you need to have an account in Desk-Net to make use of the
plugin.

You are working for an editorial or content marketing team of 15 or more users?
Learn more about Desk-Net and request your free test account on the Desk-Net
website.
