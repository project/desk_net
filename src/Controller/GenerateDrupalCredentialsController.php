<?php

namespace Drupal\desk_net\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for the Example module.
 */
class GenerateDrupalCredentialsController extends ControllerBase {

  /**
   * The generate new API credentials.
   */
  public static function generateApiCredentials() {
    // Generate a unique ID.
    $desk_net_api_key = str_replace('.', '', uniqid('api_', TRUE));
    $desk_net_api_secret = str_replace('.', '', uniqid('', TRUE));

    // Generate new clients.
    \Drupal::configFactory()
      ->getEditable('desk_net.settings')
      ->set('desk_net_api_key', $desk_net_api_key)
      ->set('desk_net_api_secret', $desk_net_api_secret)
      ->save();

    return new RedirectResponse(Url::fromUserInput('/admin/config/desk-net?dn-credentials=generate')
      ->toString());
  }

}
