<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Drupal\desk_net\Controller\RequestsController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements the Authorize Form.
 */
class DeskNetCredentialsForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The requests controller.
   *
   * @var \Drupal\desk_net\Controller\RequestsController
   */
  protected $requestsController;

  /**
   * Constructs a DeskNetCredentialsForm form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\desk_net\Controller\RequestsController $requests_controller
   *   The requests controller.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation, RequestsController $requests_controller) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->requestsController = $requests_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation'),
    $container->get('desk_net.requests_controller')
     );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_credentials';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('desk_net.settings');

    $html = '<h2>' . $this->stringTranslation->translate('Desk-Net Credentials') . '</h2>';
    $html .= $this->stringTranslation->translate('Enter here the API credentials of your Desk-Net account (not your personal Desk-Net user credentials). If you do not have these API credentials please request them from');
    $html .= ' <a href="mailto:support@desk-net.com">support@desk-net.com</a>';
    $html .= '<p><strong>' . $this->stringTranslation->translate('Please note:') . '</strong> ' .
                 $this->stringTranslation->translate('Desk-Net module relies on external configurations. Ensure these settings are properly adjusted for optimal performance') . '</p>';

    $form['html'] = [
      '#markup' => $html,
    ];

    $form['desk_net_api_user'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Desk-Net API Login'),
      '#default_value' => $config->get('desk_net_api_user'),
      '#size' => 30,
      '#maxlength' => 50,
      '#required' => TRUE,
    ];

    $form['desk_net_api_password'] = [
      '#type' => 'password',
      '#title' => $this->stringTranslation->translate('Desk-Net API Password'),
      '#size' => 30,
      '#maxlength' => 50,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->stringTranslation->translate('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValues())) {
      $api_user = $form_state->getValue('desk_net_api_user');
      $api_password = $form_state->getValue('desk_net_api_password');

      if (!empty($api_user) && !empty($api_password)) {
        $token = $this->requestsController->getToken($api_user, $api_password);

        if ($token) {
          $this->messenger->addStatus(NoticesCollection::getNotice(7));

          $this->configFactory->getEditable('desk_net.settings')
            ->set('desk_net_api_user', $api_user)
            ->set('desk_net_api_password', $api_password)
            ->save();
        }
        else {
          $this->messenger->addError(NoticesCollection::getNotice(8));
        }
      }
    }
  }

}
