<?php

namespace Drupal\desk_net\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\desk_net\Collection\NoticesCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring Desk-Net API interaction settings.
 */
class DeskNetApiInteractionSettingsForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Constructs a DeskNetApiInteractionSettingsForm form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('messenger'),
    $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'desk_net_api_interaction_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $html = '<h2>' . $this->stringTranslation->translate('API Interaction Settings') . '</h2>';
    $html .= $this->stringTranslation->translate('Configure settings for updates and deleted elements from Desk-Net 
      to Drupal, ensuring the seamless integration with the Dek-Net application.');

    $form['html'] = [
      '#markup' => $html,
    ];

    $sync_updates_from_desk_net = (bool) $this->configFactory->get('desk_net.settings')
      ->get('sync_updates_from_desk_net') ?? TRUE;

    $form['sync_updates_from_desk_net'] = [
      '#type' => 'checkbox',
      '#title' => $this->stringTranslation->translate('Synch Updates from Desk-Net'),
      '#default_value' => $sync_updates_from_desk_net,
      '#required' => FALSE,
    ];

    $sync_deletions_from_desk_net = (bool) $this->configFactory->get('desk_net.settings')
      ->get('sync_deletions_from_desk_net') ?? TRUE;

    $form['sync_deletions_from_desk_net'] = [
      '#type' => 'checkbox',
      '#title' => $this->stringTranslation->translate('Synch Deletions from Desk-Net'),
      '#default_value' => $sync_deletions_from_desk_net,
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->stringTranslation->translate('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValues())) {
      // Retrieve the values of the checkbox fields.
      $sync_updates_from_desk_net = $form_state->getValue('sync_updates_from_desk_net');
      $sync_deletions_from_desk_net = $form_state->getValue('sync_deletions_from_desk_net');

      // Save the values to configuration.
      $this->configFactory->getEditable('desk_net.settings')
        ->set('sync_updates_from_desk_net', $sync_updates_from_desk_net)
        ->set('sync_deletions_from_desk_net', $sync_deletions_from_desk_net)
        ->save();

      $this->messenger->addStatus(NoticesCollection::getNotice(13));
    }
  }

}
